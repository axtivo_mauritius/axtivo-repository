<?php
session_start();

if(isset($_SESSION['axt_auth_user'])){
	header('Location: activities.php');
	exit;

}

include_once('includes/header.php');
include_once('includes/menu.php');

// Include the random string file for captcha
require 'includes/rand.php';

// Set the session contents
$_SESSION['captcha_id'] = $str;

?>

<script>
$(document).ready(function(){

  $('.tooltip-msg').tooltip({
				selector: '[data-toggle=tooltip]',
				container: 'body'
			  });


	$("#btnCaptchaReload").click(function() {

		$("#captcha_img").load("index.php #captcha_img").fadeOut(1000).fadeIn(1000);
	});

        var validator = $("#register-form").bind("invalid-form.validate", function() {
			$("#summary").html("<div class=\"alert alert-danger\">Your form contains " + validator.numberOfInvalids() + " errors, see details below.</div>");
          }).validate({
			debug: true,
			errorElement: "em",
			errorContainer: $("#summary"),
			errorPlacement: function(error, element) {
                          error.appendTo( element.parent("dd").next("dd") );
      			},
                             highlight: function(element) {
				$(element).closest('dl').addClass('has-error');

			},
			unhighlight: function(element) {
				$(element).closest('dl').removeClass('has-error');
				$(element).closest('dl').addClass('has-success');




			},
                        submitHandler: function(form) {


							var dataString = $("#register-form").serialize();

							//alert(dataString);

                           	$.ajax({
								type: "POST",
								url: "requests/create-account.php",
								data: dataString,
								cache: false,
								beforeSend: function()
								{

									$("#response").html('<p class="alert alert-warning col-xs-12">Registration In Progress...<img src="images/loading.gif" align="absmiddle"></p>');
								},
								success: function(response)
								{
				 					$('#register-div').hide();
									$('#response').html(unescape(response));
								}

							});





                       },

                        valid:function(element) {

                                    },
			rules: {

				 		user_type_id:{required:true},
						firstname:{required:true},
						surname:{required:true},
						email:{required:true},
						telephone:{required:true},
						telephone:{required:true},
						address_1:{required:true},
						address_2:{required:true},
						business_name:{required:true},
						website:{required:true},
						password:{required:true},
						agree:{required:true},

						captcha: {
							required: true,
							remote: "includes/process.php"
							}


			},
                 	messages: {
                			captcha: "Verification code does not match"
                                }
		});


});
</script>

<div id="register" class="inside-page">
    <div class="container">


         <h1>Register as a Member or as a Host</h1>

        <div id="register-div">
          <form action="" method="post" class="col-md-6" id="register-form" name="register-form" >

                      <dl class="dl-horizontal">
                         <dt><label>Account Type</label></dt>
                         <dd>
                           <div data-toggle="buttons" class="btn-group btn-toggle">
                                        <label onclick="$('.host-dl').hide('slow');" data-toggle-class="btn-primary" class="btn btn-default">
                                          <input type="radio" value="1" name="user_type_id"> Members
                                        </label>
                                        <label onclick="$('.host-dl').show('slow');" data-toggle-class="btn-primary" class="btn btn-default">
                                          <input type="radio" value="2" name="user_type_id"> Hosts
                                        </label>


                                      </div>
                         </dd>
                         <dd></dd>
                	 </dl>

                     <dl class="dl-horizontal">
                         <dt><label>First Name</label></dt>
                         <dd>
                            <input type="text" name="firstname" id="firstname" size="30" class="form-control"  />
                         </dd>
                         <dd></dd>
                	 </dl>

                     <dl class="dl-horizontal">
                         <dt><label>Surname</label></dt>
                         <dd>
                            <input type="text" name="surname" id="surname" size="30" class="form-control"  />
                         </dd>
                         <dd></dd>
                	 </dl>

                       <dl class="dl-horizontal">
                         <dt><label>Email Address</label></dt>
                         <dd>
                            <input type="text" name="email" id="email" size="30" class="form-control"  />
                         </dd>
                         <dd></dd>
                	 </dl>

                     <dl class="dl-horizontal">
                         <dt><label>Telephone Number</label></dt>
                         <dd>
                            <input type="text" name="telephone" id="telephone" size="30" class="form-control"  />
                         </dd>
                         <dd></dd>
                	 </dl>

                      <dl class="dl-horizontal">
                         <dt><label>Address</label></dt>
                         <dd>
                            <input type="text" name="address_1" id="address_1" size="30" class="form-control"  />
                         </dd>
                         <dd></dd>
                	 </dl>

                     <dl class="dl-horizontal">
                         <dt>&nbsp;  </dt>
                         <dd>
                            <input type="text" name="address_2" id="address_2" size="30" class="form-control"  />
                         </dd>
                         <dd></dd>
                	 </dl>

                      <dl class="dl-horizontal">
                         <dt><label>Password</label></dt>
                         <dd>
                            <input type="password" name="password" id="password" size="30" class="form-control"  />
                         </dd>
                         <dd></dd>
                	 </dl>

                      <dl class="dl-horizontal host-dl" style="display:none;">
                         <dt><label>Business Name</label></dt>
                         <dd>
                            <input type="text" name="business_name" id="business_name" size="30" class="form-control"  />
                         </dd>
                         <dd></dd>
                	 </dl>

                     <dl class="dl-horizontal host-dl" style="display:none;">
                         <dt><label>Website (None? Type no)</label></dt>
                         <dd>
                            <input type="text" name="website" id="website" size="30" class="form-control"  />
                         </dd>
                         <dd></dd>
                	 </dl>

<!--
									 <dl class="dl-horizontal host-dl" style="display:none;">
											<dt><label>Bank Name</label></dt>
											<dd>
												 <input type="text" name="bank_name" id="bank_name" size="30" class="form-control"  />
											</dd>
											<dd></dd>
							 </dl>

									<dl class="dl-horizontal host-dl" style="display:none;">
											<dt><label>Short Code</label></dt>
											<dd>
												 <input type="text" name="short_code" id="short_code" size="30" class="form-control"  />
											</dd>
											<dd></dd>
							 </dl>

							 <dl class="dl-horizontal host-dl" style="display:none;">
									 <dt><label>Account Number</label></dt>
									 <dd>
											<input type="text" name="account_number" id="account_number" size="30" class="form-control"  />
									 </dd>
									 <dd></dd>
						</dl>


						  <dl class="dl-horizontal host-dl" style="display:none;">
										<dt><label>Cost Per Class/Course</label></dt>
										<dd>
												<input type="text" name="cost" id="cost" size="30" class="form-control"  />
										</dd>
										<dd></dd>
								</dl> -->

                       <dl class="dl-horizontal">
                                <dt><label for="captcha">Verification Code

                                <span class="tooltip-msg">

    <a data-toggle="tooltip" class="tooltip-info" href="#dialog-message" rel="tooltip" data-placement="top" data-original-title="Please enter the words you see in the box below. If you are not sure what the words are, click on the image to get a new set of words." style="color:#F8971D; background:none; border:none;"><i class="fa fa-info-circle"></i></a>

</span>

</label></dt>
                                <dd>
                <div id="captchaimage" class="input-group">


                             <span id="captcha_img">
                                  <img style="border:1px solid #d3cfce; border-radius:0; cursor:pointer;" src="captcha/image.php?<?php echo time(); ?>" alt="Captcha image" width="300" height="92" align="left" name="btnCaptchaReload" id="btnCaptchaReload" />
                                    </span>




      </div>

                                </dd>

                            </dl>
                                 <dl class="dl-horizontal">
                                <dt></dt>
                                <dd>

                                    <input type="text" name="captcha" maxlength="6"  id="captcha" size="17" class="form-control"  />

                                </dd>
                                <dd></dd>
                            </dl>

                     <dl class="dl-horizontal">
                         <dt>&nbsp; </dt>
                         <dd>
                           <input type="checkbox" value="1" id="agree" name="agree"> I agree to Axtivo's <a href="terms.php" target="_blank">Terms</a> and <a href="privacy-policy.php" target="_blank">Privacy Policy</a>.
                         </dd>
                         <dd></dd>
                	 </dl>

                     <dl class="dl-horizontal">
                         <dt>&nbsp; </dt>
                         <dd>
                               <button class="btn btn-primary btn-lg" type="submit" name="register-btn" id="register-btn"><span>Register</span> <i class="fa fa-angle-right"></i></button>
                         </dd>
                         <dd></dd>
                	 </dl>

            </form>
            </div>
            <div id="response"> </div>

     </div>
 </div>


 <?php include_once('includes/footer.php'); ?>
