<?php 
session_start();
 
			$old_user = $_SESSION['axt_auth_user'];
			
			unset($_SESSION['axt_auth_user']);
			$result_dest = session_destroy();
					
					if (!empty($old_user)) {
						  if ($result_dest)  {
							// if they were logged in and are now logged out
							 header('Location: ../user-dashboard.php?logout');
						  } else {
						   // they were logged in and could not be logged out
							 header('Location: ../user-dashboard.php?logout');
						 }
  					} else {
				 	 // if they weren't logged in but came to this page somehow
				 	 echo 'You are not authorised to view this page.';
				  
			}
 
  ?>

