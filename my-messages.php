<?php 
session_start();
 
include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');

if(!isset($_SESSION['axt_auth_user'])){
	die('You are not authorised to view this page.');
}
 
?>

<script>
$(document).ready(function(){	
 
});		  
</script>

<div class="inside-page">
    <div class="container">
        
         <h1>MY MESSAGES</h1>  
         
         <?php
		 
		 if($_SESSION['user_type_id']==2){
		  $messagesSQL = mysqli_query($conn, "select * from MESSAGES where host_id=".$_SESSION['user_id']." order by id desc");
		 }
		 else{ 
		  $messagesSQL = mysqli_query($conn, "select * from MESSAGES where member_id=".$_SESSION['user_id']." order by id desc");
		 }
		 
		 if (mysqli_num_rows($messagesSQL)!=0) {
			 echo '<div id="no-more-tables">
           					 <table class="col-md-12 table-bordered table-striped table-condensed cf">
									<thead class="cf">
									
									  <tr>
										<th>#</th>
										<th>Activity </th>
										<th>'.($_SESSION['user_type_id']==2?"Member":"Host").'</th>
										<th>Subject</th>
										<th>Email Sent</th>
										<th>Action</th>
									  </tr>
									  
									</thead>
									<tbody>';
							 $i = 0; 		
							 
							
							
							while ($messagesArray = mysqli_fetch_assoc($messagesSQL))  {
								
							$bookingSQL = mysqli_query($conn, "SELECT * FROM BOOKING where id=".$messagesArray['booking_id']);
							if(mysqli_num_rows($bookingSQL)>0){
								$bookingArray = mysqli_fetch_assoc($bookingSQL);	
							}
 
							$listingSQL = mysqli_query($conn, "SELECT * FROM LISTING where id=".$bookingArray['listing_id']);
							if(mysqli_num_rows($listingSQL)>0){
								$listingArray = mysqli_fetch_assoc($listingSQL);
							}
							
							$userSQL = mysqli_query($conn, "SELECT * FROM USER where id=".$bookingArray['host_id']);
							if(mysqli_num_rows($userSQL)>0){
								$userArray = mysqli_fetch_assoc($userSQL);
							}
							
							if($_SESSION['user_type_id']==2){
									$customerSQL = mysqli_query($conn, "SELECT * FROM USER where id=".$messagesArray['member_id']);
									if(mysqli_num_rows($customerSQL)>0){
										$customerArray = mysqli_fetch_assoc($customerSQL);	
									}
							}
	
 								
								echo '  <tr>
											<td data-title="#">'.$messagesArray['id'].'</td>
											<td data-title="Activity">'.$listingArray['listing_name'].'</td>
											<td data-title="'.($_SESSION['user_type_id']==2?"Member":"Host").'">'.($_SESSION['user_type_id']==2?$customerArray['firstname']." ".$customerArray['surname']:$userArray['business_name']).'</td>
											<td data-title="Subject">'.$messagesArray['subject'].'</td>
											<td data-title="Email Sent">'.date('d-m-Y',strtotime($messagesArray['date_sent'])).'</td>
											
 
											<td data-title="Action">
											
				 
				<form action="view-message.php" method="post" style="margin-right:5px">
					<input type="hidden" name="id" value="'.$messagesArray['id'].'">
					<input type="hidden" name="btn-action" value="view">
					<button class="btn btn-default btn-action" type="submit"><i class="fa fa-eye"></i> View Message</button>
				</form>
				
				 
											</td>
										</tr>';
										 $i++; 
									}
							echo '</tbody>
								  </table>
								  </div> 
								 ';
		 }
		 
		 else {
						echo '<div class="alert alert-warning">
           
             
            <p>You do not have any messages.</p> 
 
          </div>';
						}
		
		 
		 ?>
      	
            
         
        
     </div>  
 </div>
 
 
 <?php include_once('includes/footer.php'); ?>