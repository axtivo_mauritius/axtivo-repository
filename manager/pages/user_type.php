<?php
$model = $_GET['model'];
$id = isset($_GET['id']) ? $_GET['id'] : 0;

if(isset($_GET['action'])){
	if($_GET['action'] == "save"){

		$data = array();
        $data = $_POST;

        $erreur = "";
        if(empty($data['type_name'])){
        	$erreur = "User type name empty";
        }

        if(empty($erreur)){
        	if($data['id'] > 0){
        		$sqlUpdateUserType = "UPDATE USER_TYPE SET type_name = '".$data['type_name']."' WHERE id = '".$data['id']."'";
        		$resultUpdateUserType = mysqli_query($conn, $sqlUpdateUserType);
	        }else{
	        	$sqlInsertUserType = "INSERT INTO USER_TYPE (id, type_name) VALUES('', '".$data['type_name']."')";
	        	$resultInsertUserType = mysqli_query($conn, $sqlInsertUserType);
	        }

        	header('location: list.php?success=1&model='.$model );
        }

	}elseif($_GET['action'] == "del"){
		$sqlDelUserType = "DELETE FROM USER_TYPE WHERE id = '".$id."'";
        $resultDelUserType = mysqli_query($conn, $sqlDelUserType);
		header('location: list.php?delsuccess=1&model='.$model );
	}
}

include_once('includes/header.php');
include_once('includes/security.php');
include_once('includes/sidebar.php');
include_once('includes/navbar.php');

if($id == 0) {
    echo '<h2 class="page-header">Add New</h2>';
}
else {
    echo '<h2 class="page-header">Edit</h2>';
}

if($id > 0){
	$sqlListUserType = "SELECT * FROM USER_TYPE WHERE id = '".$id."'";
	$resultListUserType = mysqli_query($conn, $sqlListUserType);
	$countListUserType = mysqli_num_rows($resultListUserType);
	$data = array();
	if($countListUserType > 0){
		$dataListUserType = mysqli_fetch_assoc($resultListUserType);
		$data = $dataListUserType;
	}
}
?>


<form enctype="multipart/form-data" method="post" action="?model=user_type&amp;action=save" id="edit" class="horizontal" role="form">
	<div class="form-group col-lg-12">
		<div class="row">
			<div class="col-lg-7">

				<div class = "row">
					<label class="col-sm-3 control-label" for="id">ID</label>
					<div class="col-sm-9">
						<?php echo $data['id']; ?>
						<input type="hidden" value="<?php echo $data['id']; ?>" name="id"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="type_name"> Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['type_name']; ?>" name="type_name"> 
					</div>
				</div>



				<div class="row">
					<button class="btn btn-warning" type="submit">Submit</button>
				</div>
			</div>
		</div>
	</div>
</form>

<?php
include_once('includes/footer.php');
?>