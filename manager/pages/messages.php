<?php
$model = $_GET['model'];
$id = isset($_GET['id']) ? $_GET['id'] : 0;

if(isset($_GET['action'])){
}

include_once('includes/header.php');
include_once('includes/security.php');
include_once('includes/sidebar.php');
include_once('includes/navbar.php');

if($id == 0) {
    echo '<h2 class="page-header">Add New</h2>';
}
else {
    echo '<h2 class="page-header">Edit</h2>';
}

if($id > 0){
	$sqlListMessages = "SELECT * FROM MESSAGES WHERE id = '".$id."'";
	$resultListMessages = mysqli_query($conn, $sqlListMessages);
	$countListMessages = mysqli_num_rows($resultListMessages);
	$data = array();
	if($countListMessages > 0){
		$dataListMessages = mysqli_fetch_assoc($resultListMessages);
		$data = $dataListMessages;
	}
}
?>

<form enctype="multipart/form-data" method="post" action="?model=messages&amp;action=save" id="edit" class="horizontal" role="form">
	<div class="form-group col-lg-12">

		<div class="row">

			<div class="col-lg-7">

				<div class="row">
					<label class="col-sm-3 control-label" for="id">ID</label>
					<div class="col-sm-9">
						<?php echo $data['id']; ?>
						<input type="hidden" value="<?php echo $data['id']; ?>" name="id"> 
					</div>
				</div>


				<div class="row">
					<label class="col-sm-3 control-label" for="member_id">Recipient</label>
					<div class="col-sm-9">
						<select class="form-control" name="member_id"> 
							<option value="6">mcenaneys@icloud.com</option>
							<option value="7">wandlewed@gmail.com</option>
						</select>
					</div>
				</div>


				<div class="row">
					<label class="col-sm-3 control-label" for="subject">Subject</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="New Booking" name="subject"> 
					</div>
				</div>


				<div class="row">
					<label class="col-sm-3 control-label" for="date_sent">Date Sent</label>
					<div class="col-sm-9">
						<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
						<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
						<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
						<script type="text/javascript">
						$(document).ready(function(){
						$(".date_field_date_sent").datepicker();
						});
						</script>
						<input type="text" class="date_field_date_sent form-control hasDatepicker" value="<?php echo $data['date_sent']; ?>" name="date_sent" id="dp1499168798223"> 
					</div>
				</div>


				<div class="row">
					<label class="col-sm-3 control-label" for="booking_id">Booking ID</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['booking_id']; ?>" name="booking_id"> 
					</div>
				</div>


				<div class="row">
					<label class="col-sm-3 control-label" for="host_id">Host</label>
					<div class="col-sm-9">
						<select class="form-control" name="host_id"> 
							<option value="6"></option>
							<option value="7"></option>
						</select>
					</div>
				</div>



				<div class="row">
					<button class="btn btn-warning" type="submit">Submit</button>
				</div>


			</div>
		</div>
	</div>
</form>

<?php
include_once('includes/footer.php');
?>