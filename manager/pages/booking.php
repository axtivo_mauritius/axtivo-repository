<?php
$model = $_GET['model'];
$id = isset($_GET['id']) ? $_GET['id'] : 0;

if(isset($_GET['action'])){
	if($_GET['action'] == "save"){
		$data = array();
        $data = $_POST;
        if($data['id'] > 0){
        	$sqlUpdateBooking = "UPDATE BOOKING SET listing_id = '".$data['listing_id']."', user_id = '".$data['user_id']."', host_id = '".$data['host_id']."', booking_date = '".$data['booking_date']."' WHERE id = '".$data['id']."'";
        	$resultUpdateBooking = mysqli_query($conn, $sqlUpdateBooking);
        }else{
        	$sqlInsertBooking = "INSERT INTO BOOKING (id, listing_id, user_id, host_id, booking_date) VALUES ('', '".$data['listing_id']."', '".$data['user_id']."', '".$data['host_id']."', '".$data['booking_date']."')";
        	$resultInsertBooking = mysqli_query($conn, $sqlInsertBooking);
        }
		header('location: list.php?success=1&model='.$model );
	}elseif($_GET['action'] == "del"){
		$sqlDelBooking = "DELETE FROM BOOKING WHERE id = '".$id."'";
        $resultDelBooking = mysqli_query($conn, $sqlDelBooking);
		header('location: list.php?delsuccess=1&model='.$model );
	}
}

include_once('includes/header.php');
include_once('includes/security.php');
include_once('includes/sidebar.php');
include_once('includes/navbar.php');

if($id == 0) {
    echo '<h2 class="page-header">Add New</h2>';
}
else {
    echo '<h2 class="page-header">Edit</h2>';
}

if($id > 0){
	$sqlListBooking = "SELECT * FROM BOOKING WHERE id = '".$id."'";
	$resultListBooking = mysqli_query($conn, $sqlListBooking);
	$countListBooking = mysqli_num_rows($resultListBooking);
	$data = array();
	if($countListBooking > 0){
		$dataListBooking = mysqli_fetch_assoc($resultListBooking);
		$data = $dataListBooking;
	}
}
?>

<form enctype="multipart/form-data" method="post" action="?model=booking&amp;action=save" id="edit" class="horizontal" role="form">
	<div class="form-group col-lg-12">

		<div class="row">
			<div class="col-lg-7">


			<div class = "row">
				<label class="col-sm-3 control-label" for="id">ID</label>
				<div class="col-sm-9">
					<?php echo $data['id']; ?>
					<input type="hidden" value="<?php echo $data['id']; ?>" name="id"> 
				</div>
			</div>

<?php 
			//Listing
            $sqlListing = "SELECT * FROM LISTING WHERE 1 = 1";
            $resultListing = mysqli_query($conn,$sqlListing);
            $countListing = mysqli_num_rows($resultListing);
?>
			<div class = "row">
				<label class="col-sm-3 control-label" for="listing_id">Listing</label>
				<div class="col-sm-9">
					<select class="form-control" name="listing_id"> 
<?php 
						while($dataListing = mysqli_fetch_assoc($resultListing)){
							$selected = "";
                            if($data['listing_id'] == $dataListing['id']){
                                $selected = "selected = 'selected'";
                            }
?>
							<option value="<?php echo $dataListing['id'] ?>" <?php echo $selected; ?>><?php echo $dataListing['listing_name'] ?></option>
<?php 
						}
?>
					</select>
				</div>
			</div>

<?php 
			//User
            $sqlUser = "SELECT * FROM USER WHERE 1 = 1";
            $resultUser = mysqli_query($conn,$sqlUser);
            $countUser = mysqli_num_rows($resultUser);
?>
			<div class = "row">
				<label class="col-sm-3 control-label" for="user_id">User</label>
				<div class="col-sm-9">
					<select class="form-control" name="user_id"> 
<?php 
						while($dataUser = mysqli_fetch_assoc($resultUser)){
							$selected = "";
                            if($data['user_id'] == $dataUser['id']){
                                $selected = "selected = 'selected'";
                            }
?>
							<option value="<?php echo $dataUser['id']; ?>" <?php echo $selected; ?>><?php echo $dataUser['email']; ?></option>
<?php 
						}
?>
					</select>
				</div>
			</div>

<?php 
			//HOST
            $sqlHost = "SELECT * FROM USER WHERE 1 = 1";
            $resultHost = mysqli_query($conn,$sqlHost);
            $countHost = mysqli_num_rows($resultHost);
?>

			<div class = "row">
				<label class="col-sm-3 control-label" for="host_id">Host</label>
				<div class="col-sm-9">
					<select class="form-control" name="host_id"> 
<?php 
						while($dataHost = mysqli_fetch_assoc($resultHost)){
							$selected = "";
                            if($data['host_id'] == $dataHost['id']){
                                $selected = "selected = 'selected'";
                            }
?>
							<option value="<?php echo $dataHost['id']; ?>" <?php echo $selected; ?>><?php echo $dataHost['business_name']; ?></option>
<?php
						}
?>
					</select>
				</div>
			</div>


			<div class = "row">
				<label class="col-sm-3 control-label" for="booking_date">Booking Date</label>
				<div class="col-sm-9">
					<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
					<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
					<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
					<script type="text/javascript">
						$(document).ready(function(){
							$(".date_field_booking_date").datepicker();
						});
					</script>
					<input type="text" class="date_field_booking_date form-control hasDatepicker" value="<?php echo $data['booking_date']; ?>" name="booking_date" id="dp1499165472587"> 
				</div>
			</div>



			<div class="row">
				<button class="btn btn-warning" type="submit">Submit</button>
			</div>


			</div>
		</div>
	</div>
</form>

<?php
include_once('includes/footer.php');
?>