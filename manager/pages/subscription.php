<?php
$model = $_GET['model'];
$id = isset($_GET['id']) ? $_GET['id'] : 0;

if(isset($_GET['action'])){
	if($_GET['action'] == "save"){
		$data = array();
        $data = $_POST;

        if($data['id'] > 0){
        	$sqlUpdateSubscription = "UPDATE SUBSCRIPTION SET subscription_name = '".$data['subscription_name']."', start_date = '".$data['start_date']."', expiration_date = '".$data['expiration_date']."', credits_purchased = '".$data['credits_purchased']."', credits_remaining = '".$data['credits_remaining']."', user_id = '".$data['user_id']."' WHERE id = '".$data['id']."'";
        	$resultUpdateSubscription = mysqli_query($conn, $sqlUpdateSubscription);
        }else{
        	$sqlInsertSubscription = "INSERT INTO SUBSCRIPTION (id, subscription_name, start_date, expiration_date, credits_purchased, credits_remaining, user_id) VALUES('', '".$data['subscription_name']."', '".$data['start_date']."', '".$data['expiration_date']."', '".$data['credits_purchased']."', '".$data['credits_remaining']."', '".$data['user_id']."')";
        	$resultInsertSubscription = mysqli_query($conn, $sqlInsertSubscription);
        }
        header('location: list.php?success=1&model='.$model );

	}elseif($_GET['action'] == "del"){
		$sqlDelSubscription = "DELETE FROM SUBSCRIPTION WHERE id = '".$id."'";
        $resultDelSubscription = mysqli_query($conn, $sqlDelSubscription);
		header('location: list.php?delsuccess=1&model='.$model );
	}
}

include_once('includes/header.php');
include_once('includes/security.php');
include_once('includes/sidebar.php');
include_once('includes/navbar.php');

if($id == 0) {
    echo '<h2 class="page-header">Add New</h2>';
}
else {
    echo '<h2 class="page-header">Edit</h2>';
}

if($id > 0){
	$sqlListSubscription = "SELECT * FROM SUBSCRIPTION WHERE id = '".$id."'";
	$resultListSubscription = mysqli_query($conn, $sqlListSubscription);
	$countListSubscription = mysqli_num_rows($resultListSubscription);
	$data = array();
	if($countListSubscription > 0){
		$dataListSubscription = mysqli_fetch_assoc($resultListSubscription);
		$data = $dataListSubscription;
	}
}
?>

<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".date_field_start_date").datepicker();
		$(".date_field_expiration_date").datepicker();
	});
</script>


<form enctype="multipart/form-data" method="post" action="?model=subscription&amp;action=save" id="edit" class="horizontal" role="form">

	<div class="form-group col-lg-12">

		<div class="row">

			<div class="col-lg-7">

				<div class="row">
					<label class="col-sm-3 control-label" for="id">ID</label>
					<div class="col-sm-9">
						<?php echo $data['id']; ?>
						<input type="hidden" value="<?php echo $data['id']; ?>" name="id"> 
					</div>
				</div>

<?php 
				$sqlListUser = "SELECT * FROM USER WHERE 1 = 1";
				$resultListUser = mysqli_query($conn, $sqlListUser);
				$countListUser = mysqli_num_rows($resultListUser);
?>

				<div class="row">
					<label class="col-sm-3 control-label" for="user_id">User</label>
					<div class="col-sm-9">
						<select class="form-control" name="user_id"> 
<?php
							while ($dataListUser = mysqli_fetch_assoc($resultListUser)) {
								$selected = "";
								if($data['user_id'] == $dataListUser['id']){
									$selected = "selected='selected'";
								}
?>
								<option value="<?php echo $dataListUser['id']; ?>" <?php echo $selected; ?>>
									<?php echo $dataListUser['email']; ?>
								</option>
<?php 
							 } 
?>
						</select>
					</div>
				</div>


				<div class="row">
					<label class="col-sm-3 control-label" for="subscription_name">Subscription Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['subscription_name']; ?>" name="subscription_name"> 
					</div>
				</div>


				<div class="row">
					<label class="col-sm-3 control-label" for="start_date">Start Date</label>
					<div class="col-sm-9">
						<input type="text" class="date_field_start_date form-control hasDatepicker datepicker" value="<?php echo $data['start_date']; ?>" name="start_date" id="dp1499159544469"> 
					</div>
				</div>


				<div class="row">
					<label class="col-sm-3 control-label" for="expiration_date">Expiration Date</label>
					<div class="col-sm-9">
						<input type="text" class="date_field_expiration_date form-control hasDatepicker datepicker" value="<?php echo $data['expiration_date']; ?>" name="expiration_date" id="dp1499159544470"> 
					</div>
				</div>


				<div class="row">
					<label class="col-sm-3 control-label" for="credits_purchased">Credits Purchased</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['credits_purchased']; ?>" name="credits_purchased"> 
					</div>
				</div>


				<div class="row">
					<label class="col-sm-3 control-label" for="credits_remaining">Credits Remaining</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['credits_remaining']; ?>" name="credits_remaining"> 
					</div>
				</div>



				<div class="row">
					<button class="btn btn-warning" type="submit">Submit</button>
				</div>


			</div>
		</div>
	</div>
</form>

<?php
include_once('includes/footer.php');
?>