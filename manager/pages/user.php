<?php
$model = $_GET['model'];
$id = isset($_GET['id']) ? $_GET['id'] : 0;

if(isset($_GET['action'])){
	if($_GET['action'] == "save"){

		$data = array();
        $data = $_POST;

        $erreur = "";
        if($data['id'] > 0){
        	if(empty($data['password']) || empty($data['pass_confirm'])){
        		$sqlUpdateUser = "UPDATE USER SET firstname = '".$data['firstname']."', surname = '".$data['surname']."', email = '".$data['email']."', telephone = '".$data['telephone']."', address_1 = '".$data['address_1']."', address_2 = '".$data['address_2']."', business_name = '".$data['business_name']."', website = '".$data['website']."', bank_name = '".$data['bank_name']."', short_code = '".$data['short_code']."', account_number = '".$data['account_number']."', cost = '".$data['cost']."', status = '".$data['status']."', user_type_id = '".$data['user_type_id']."' WHERE id = '".$data['id']."'
        		";
        		$resultUpdateUser = mysqli_query($conn,$sqlUpdateUser);
        	}elseif($data['password'] != $data['pass_confirm']){
        		$erreur = "Password not equal";
        	}else{
    			$data['password'] = hash('sha512', $data['password']);

        		$sqlUpdateUser = "UPDATE USER SET firstname = '".$data['firstname']."', surname = '".$data['surname']."', email = '".$data['email']."', telephone = '".$data['telephone']."', address_1 = '".$data['address_1']."', address_2 = '".$data['address_2']."', password = '".$data['password']."', business_name = '".$data['business_name']."', website = '".$data['website']."', bank_name = '".$data['bank_name']."', short_code = '".$data['short_code']."', account_number = '".$data['account_number']."', cost = '".$data['cost']."', status = '".$data['status']."', user_type_id = '".$data['user_type_id']."' WHERE id = '".$data['id']."'
        		";
        		$resultUpdateUser = mysqli_query($conn,$sqlUpdateUser);
        	}

        	if(empty($erreur)){
        		header('location: list.php?success=1&model='.$model );
        	}
        	
        }else{
        	if(empty($data['password']) || empty($data['pass_confirm'])){
        		$erreur = "Password empty";
        	}
        	elseif($data['password'] != $data['pass_confirm']){
        		$erreur = "Password not equal";
        	}else{
        		$data['password'] = hash('sha512', $data['password']);
        	}

        	if(empty($erreur)){
	        		$sqlInsertUser = "INSERT INTO USER (id, firstname, surname, email, telephone, address_1, address_2, 	password, business_name, website, bank_name, short_code, account_number, cost, status, user_type_id) VALUES ('', '".$data['firstname']."', '".$data['surname']."', '".$data['email']."', '".$data['telephone']."', '".$data['address_1']."', '".$data['address_2']."', '".$data['password']."', '".$data['business_name']."', '".$data['website']."', '".$data['bank_name']."', '".$data['short_code']."', '".$data['account_number']."', '".$data['cost']."', '".$data['status']."', '".$data['user_type_id']."')";
	        	$resultInsertUser = mysqli_query($conn,$sqlInsertUser);
	        	header('location: list.php?success=1&model='.$model );
        	}
        }
	}elseif($_GET['action'] == "del"){
		$sqlDelUser = "DELETE FROM USER WHERE id = '".$id."'";
        $resultDelUser = mysqli_query($conn, $sqlDelUser);
		header('location: list.php?delsuccess=1&model='.$model );
	}
}


include_once('includes/header.php');
include_once('includes/security.php');
include_once('includes/sidebar.php');
include_once('includes/navbar.php');

if($id == 0) {
    echo '<h2 class="page-header">Add New</h2>';
}
else {
    echo '<h2 class="page-header">Edit</h2>';
}

if($id > 0){
	$sqlListUser = "SELECT * FROM USER WHERE id = '".$id."'";
	$resultListUser = mysqli_query($conn, $sqlListUser);
	$countListUser = mysqli_num_rows($resultListUser);
	$data = array();
	if($countListUser > 0){
		$dataListUser = mysqli_fetch_assoc($resultListUser);
		$data = $dataListUser;
	}
}
?>

<form enctype="multipart/form-data" method="post" action="?model=user&amp;action=save" id="edit" class="horizontal" role="form">
	<div class="form-group col-lg-12">

		<div class="row">
			<div class="col-lg-7">

				<div class = "row">
					<label class="col-sm-3 control-label" for="id">ID</label>
					<div class="col-sm-9">
						<?php echo $data['id']; ?>
						<input type="hidden" value="<?php echo $data['id']; ?>" name="id"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="firstname">First Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['firstname']; ?>" name="firstname"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="surname">Surname</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['surname']; ?>" name="surname"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="email">Email</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['email']; ?>" name="email"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="telephone">telephone</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['telephone']; ?>" name="telephone"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="address_1">Address Line 1</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['address_1']; ?>" name="address_1"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="address_2">Address Line 2</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['address_2']; ?>" name="address_2"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="password">Password</label>
					<div class="col-sm-9">
						Leave this 2 fields blank to leave old password.<br>
						<input type="password" value="" class="form-control" name="password"> 
						<br>
						<input type="password" value="" class="form-control" name="pass_confirm"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="business_name">Business Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['business_name']; ?>" name="business_name"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="website">Website</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['website']; ?>" name="website"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="bank_name">Bank Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['bank_name']; ?>" name="bank_name"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="short_code">Short Code</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['short_code']; ?>" name="short_code"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="account_number">Account Number</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['account_number']; ?>" name="account_number"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="cost">Cost Per Class/Course</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="<?php echo $data['cost']; ?>" name="cost"> 
					</div>
				</div>


				<div class = "row">
					<label class="col-sm-3 control-label" for="status">Status</label>
					<div class="col-sm-9">
						<select class="form-control" name="status">
<?php 
							if($data['status'] == 'Active'){
?>
								<option selected="selected" value="Active">Active</option>
<?php 
							}else{
?>
								<option value="Active">Active</option>
<?php 
							}
?> 

<?php 
							if($data['status'] == 'Suspended'){
?>
								<option selected="selected" value="Suspended">Suspended</option>
<?php 
							}else{
?>
								<option value="Suspended">Suspended</option>
<?php 
							}
?>
						</select>
					</div>
				</div>

<?php 
				$sqlUserType = "SELECT * FROM USER_TYPE WHERE 1 = 1";
				$resultUserType = mysqli_query($conn, $sqlUserType);
?>

				<div class = "row">
					<label class="col-sm-3 control-label" for="user_type_id">User Type</label>
					<div class="col-sm-9">
						<select class="form-control" name="user_type_id"> 
<?php 
							while($dataUserType = mysqli_fetch_assoc($resultUserType)){
								$selected = "";
								if($dataUserType['id'] == $data['user_type_id']){
									$selected = "selected='selected'";
								}
?>
								<option value="<?php echo $dataUserType['id']; ?>" <?php echo $selected; ?>><?php echo $dataUserType['type_name']; ?></option>
<?php 
							}
?>
						</select>
					</div>
				</div>



				<div class="row">
					<button class="btn btn-warning" type="submit">Submit</button>
				</div>


			</div>
		</div>
	</div>
</form>


<?php
include_once('includes/footer.php');
?>