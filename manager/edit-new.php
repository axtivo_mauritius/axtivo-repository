<?php 
session_start();
if(!isset($_SESSION['axt_auth_admin'])){
	die('Access Denied.');
}

include_once('../includes/mydb.php');

if(isset($_GET['model'])){
    if($_GET['model'] == 'user'){
        include "pages/user.php";
    }elseif($_GET['model'] == 'user_type'){
        include "pages/user_type.php";
    }elseif($_GET['model'] == 'subscription'){
        include "pages/subscription.php";
    }elseif($_GET['model'] == 'listing'){
        include "pages/listing.php";
    }elseif($_GET['model'] == 'booking'){
        include "pages/booking.php";
    }elseif($_GET['model'] == 'messages'){
        include "pages/messages.php";
    }elseif($_GET['model'] == 'category'){

    }elseif($_GET['model'] == 'static_text'){

    }elseif($_GET['model'] == 'admin_staff'){

    }elseif($_GET['model'] == 'location'){

    }
}