<?php 
session_start();
if(isset($_SESSION['nem_auth_admin'])){
header('Location: ../admin/overview.php');
exit();
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>AXTIVO | Staff Login</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Login style sheet -->
    <link href="assets/css/login.css" rel="stylesheet">
	
		<!-- Javascript -->
	<script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

  </head>

  <body>

    <div class="container">


      <form class="form-signin" role="form" method="post" action="requests/authenticate-admin.php">
         <div id="logo">
        	<img src="assets/images/logo.jpg" />
        </div>
        <input type="email" class="form-control" placeholder="Email Address" name="admin_email" required autofocus>
        <input type="password" class="form-control" placeholder="Password" name="admin_password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
			<?php 
	
	if (isset($_GET['error'])) {
                    echo '<br /><div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Authentication failed.</strong> </div>';
            }
			
			if (isset($_GET['logout'])) {
                    echo '<br /><div class="alert alert-success alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>You have been logged out. </div>';
            }
			?>
      </form>

    </div>
  </body>
</html>
