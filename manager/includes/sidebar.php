    <div class="container-fluid">
      <div class="row">
	    
        <div class="col-sm-3 col-md-2 sidebar-user">
			
			 <ul class="nav side-nav" id="admin-sidebar">
					<li><a href="list.php?model=user"><i class="fa fa-group"></i> User</a></li>
					<li><a href="list.php?model=user_type"><i class="fa fa-male"></i> User Type</a></li>
					<li><a href="list.php?model=subscription"> <i class="fa fa-credit-card"></i> Subscription</a></li>
					<li><a href="list.php?model=listing"> <i class="fa fa-list-alt"></i> Listing</a></li>
					<li><a href="list.php?model=booking"><i class="fa fa-calendar"></i> Booking</a></li>
					<li><a href="list.php?model=messages"><i class="fa fa-envelope"></i> Messages</a></li>
					<li><a href="list.php?model=category"><i class="fa fa-th-large"></i> Category</a></li>
					<li><a href="list.php?model=static_text"><i class="fa fa-font"></i> Static Text</a></li>
					<li><a href="list.php?model=admin_staff"><i class="fa fa-user"></i> Admin Staff</a></li>
					<li><a href="list.php?model=location"><i class="fa fa-map-marker"></i> Location</a></li>
 
		  </ul>
		  
		   
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
