<?php 

//Prevent Direct access
if(!isset($_SESSION['axt_auth_user']) && !isset($_SESSION['axt_auth_admin'])) {
	header('Location: index.php?error');
	 exit;
}

//Check if session has timed out (more than 10 mins)
$inactive = 600;
 
// check to see if $_SESSION["timeout"] is set
if (isset($_SESSION["timeout"])) {
    // calculate the session's "time to live"
    $sessionTTL = time() - $_SESSION["timeout"];
	/**
    if ($sessionTTL > $inactive) {
        //session_destroy();
		if(isset($_SESSION['nem_auth_user'])) {
		
			header("Location: requests/logout.php");
			exit;
		}
		
	 if(isset($_SESSION['nem_auth_admin'])) {
		
			header("Location: ../requests/logout.php");
			exit;
		}
    }
	**/
}
 
$_SESSION["timeout"] = time();

?>