<?php error_reporting(0); 
header('Cache-Control: max-age=900');
session_start();
$baseURL ="http://axtivo.com";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Nemesys">


    <title>AXTIVO | Admin Panel</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $baseURL; ?>/manager/assets/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $baseURL; ?>/manager/assets/font-awesome/css/font-awesome.min.css">

    <!-- Custom style -->
    <link href="<?php echo $baseURL; ?>/manager/assets/css/custom.css" rel="stylesheet">
	
	<!-- Javascript -->
	<script src="<?php echo $baseURL; ?>/manager/assets/js/jquery.js"></script>
    <script src="<?php echo $baseURL; ?>/manager/assets/js/bootstrap.min.js"></script>

    <script src="<?php echo $baseURL; ?>/manager/assets/js/jquery.tablesorter.js"></script>
    <script src="<?php echo $baseURL; ?>/manager/assets/js/tables.js"></script>

    <script src="<?php echo $baseURL; ?>/manager/assets/css/bootstrap-multiselect.css"></script>
    <script src="<?php echo $baseURL; ?>/manager/assets/js/bootstrap-multiselect.js"></script>

    <script src="<?php echo $baseURL; ?>/manager/assets/css/bootstrap-datepicker.css"></script>
    <script src="<?php echo $baseURL; ?>/manager/assets/js/bootstrap-datepicker.min.js"></script>

  </head>

  <body>


