      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="overview.php"><img src="assets/images/logo-nav.png" /></a>
        </div>
        <div class="navbar-collapse collapse">
 
		  		 
  			<ul class="nav navbar-nav navbar-right navbar-user">
	
				<li class="dropdown user-dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['first_name'].' '.$_SESSION['last_name']; ?><b class="caret"></b></a>
				  <ul class="dropdown-menu">
					<li><a href="edit.php?model=admin_staff&id=<?php echo $_SESSION['admin_id'] ?>"><i class="fa fa-user"></i> My Profile</a></li>
					<li><a href="requests/logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>
				  </ul>
				</li>
          </ul>
 	  
		 
        </div>
      </div>
    </div>
<?php 

include_once('../includes/mydb.php');
?>