<?php 
session_start();
 

if(isset($_SESSION['axt_auth_admin'])){

			$old_user = $_SESSION['axt_auth_admin'];
			
			unset($_SESSION['axt_auth_admin']);
			$result_dest = session_destroy();
					
					if (!empty($old_user)) {
						  if ($result_dest)  {
							// if they were logged in and are now logged out
							 header('Location: ../index.php?logout');
						  } else {
						   // they were logged in and could not be logged out
							 header('Location: ../overview.php');
						 }
  					} else {
				 	 // if they weren't logged in but came to this page somehow
				 	 echo 'You are not authorised to view this page.';
				  
			}
}
  ?>

