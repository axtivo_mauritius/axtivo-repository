<?php 
session_start();
if(!isset($_SESSION['axt_auth_admin'])){
    die('Access Denied.');
}

include_once('../includes/mydb.php');

$model = $_GET['model'];
$id = isset($_GET['id']) ? $_GET['id'] : 0;
$data = array();

if(isset($_GET['action']))
{
    //inserting/updating row
    if($_GET['action'] == "save"){

        $data = array();
        $data = $_POST;

        $data['id'] = $data['id'];
        $data['listing_name'] = trim($data['listing_name']);
        $data['description'] = trim($data['description']);
        $data['venue'] = trim($data['venue']);
        $data['vacancies'] = trim($data['vacancies']);

        $erreur = "";
        //Tous les champs sont obligatoires
        if(!empty($data['listing_name']) && !empty($data['description']) && !empty($data['venue']) && !empty($data['vacancies'])){

            if(count($data['category_id']) > 0){

                //Le nom original du fichier, comme sur le disque du visiteur (exemple : mon_icone.png).
                $ImgName = $_FILES['image']['name'];

                if(!empty($ImgName)){
                    //Le type du fichier. Par exemple, cela peut être « image/png ».
                    $ImgType = $_FILES['image']['type']; 

                    //La taille du fichier en octets.
                    $ImgSize = $_FILES['image']['size']; 

                    //L'adresse vers le fichier uploadé dans le répertoire temporaire.
                    $ImgTmp = $_FILES['image']['tmp_name'];

                    //Taille d'image
                    $maxsize = '104857600';//100Mo
                    if($_FILES['image']['size'] > $maxsize){
                        $erreur = "File size too large";
                    }


                    //Type des fichiers
                    $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
                    $extension_upload = strtolower(substr(strrchr($_FILES['image']['name'], '.')  ,1));
                    if(!in_array($extension_upload, $extensions_valides)){
                        $erreur = "File type invalid";
                    }

                    //Taille des fichiers
                    $maxwidth = '1200';
                    $maxheight = '1024';
                    $image_sizes = getimagesize($_FILES['image']['tmp_name']);
                    if ($image_sizes[0] > $maxwidth OR $image_sizes[1] > $maxheight){
                        $erreur = "File size too large";
                    }
                }

            }else{
                $erreur = "Please select the category";
            }
        }else{
            $erreur = "Please check all input";
        }


        if(empty($erreur)){

            $resultat = false;
            if(!empty($ImgName)){
                $nom = "../graphics/".$ImgName;
                $resultat = move_uploaded_file($_FILES['image']['tmp_name'], $nom);
            }

            if(!$resultat){
                if(!empty($data['check_image'])){
                    $resultat = true;
                    $ImgName = $data['check_image'];
                }else{
                    $ImgName = "";
                    $resultat = true;
                }
            }
            

            if($resultat){
                $data['image'] = $ImgName;

                //User connected
                $data['user_id'] = $_SESSION['admin_id'];

                //Category List
                $ListCategory = array();
                $ListCategory = $data['category_id'];

                //Insertions
                if($data['id'] == '0'){
                    //Insertion dans la table LISTING
                    $UpdateInsertListing = "INSERT INTO LISTING (id, listing_name, description, image, venue, vacancies, location_id, user_id) VALUES('', '".$data['listing_name']."', '".$data['description']."', '".$data['image']."', '".$data['venue']."', '".$data['vacancies']."', '".$data['location_id']."', '".$data['user_id']."')";
                    $resultUpdateInsertListing = mysqli_query($conn,$UpdateInsertListing);
                    $IdListingInsert = mysqli_insert_id($conn);

                    //Insertion dans la table 'LISTING_CATEGORY'
                    foreach($ListCategory as $CategoryId){
                        $UpdateInsertListingCategory = "INSERT INTO LISTING_CATEGORY (id, listing_id, category_id) VALUES('', '".$IdListingInsert."', '".$CategoryId."')";
                        $resultUpdateInsertListingCategory = mysqli_query($conn,$UpdateInsertListingCategory);
                    }
                }
                //Modifications
                else{
                    //Update dans la table LISTING
                    $UpdateInsertListing = "UPDATE LISTING SET 
                        listing_name = '".$data['listing_name']."',
                        description = '".$data['description']."',
                        image = '".$data['image']."',
                        venue = '".$data['venue']."',
                        vacancies = '".$data['vacancies']."',
                        location_id = '".$data['location_id']."',
                        user_id = '".$data['user_id']."'
                        WHERE id = '".$data['id']."'
                    ";
                    $resultUpdateInsertListing = mysqli_query($conn,$UpdateInsertListing);
                    //$IdListingInsert = mysqli_insert_id($conn);

                    //Delete Old Category list
                    $sqlDelCateg = "DELETE FROM LISTING_CATEGORY WHERE listing_id = '".$data['id']."'";
                    $resultDelCateg = mysqli_query($conn,$sqlDelCateg);

                    //Insertion dans la table 'LISTING_CATEGORY'
                    foreach($ListCategory as $CategoryId){
                        $UpdateInsertListingCategory = "INSERT INTO LISTING_CATEGORY (id, listing_id, category_id) VALUES('', '".$data['id']."', '".$CategoryId."')";
                        $resultUpdateInsertListingCategory = mysqli_query($conn,$UpdateInsertListingCategory);
                    }
                }
                header('location: list.php?success=1&model='.$model );

                //$resultUpdateInsertListing = mysqli_query($conn,$UpdateInsertListing);
            }else{
                $erreur = "Error";
            }
        }
       
        //header('location: list.php?success=1&model='.$model );
    }

    //deleting row
    if($_GET['action'] == "del"){
        if($id > 0){
            //Delete LISTING
            $sqlDelListing = "DELETE FROM LISTING WHERE id = '".$id."'";
            $resultDelListing = mysqli_query($conn,$sqlDelListing);

            //Delete Old Category list
            $sqlDelCateg = "DELETE FROM LISTING_CATEGORY WHERE listing_id = '".$id."'";
            $resultDelCateg = mysqli_query($conn,$sqlDelCateg);
        }
        header('location: list.php?delsuccess=1&model='.$model );
    }
}

if($id > 0){
    $sqlListing = "SELECT * FROM LISTING WHERE id = '".$id."' LIMIT 1";
    $resultListing = mysqli_query($conn,$sqlListing);
    $countListing = mysqli_num_rows($resultListing);
    $data = array();
    if($countListing > 0){
        $dataListing = mysqli_fetch_assoc($resultListing);
        $data['id'] = $dataListing['id'];
        $data['listing_name'] = $dataListing['listing_name'];
        $data['description'] = $dataListing['description'];
        $data['image'] = $dataListing['image'];
        $data['venue'] = $dataListing['venue'];
        $data['vacancies'] = $dataListing['vacancies'];
        $data['location_id'] = $dataListing['location_id'];

        $sqlCategoryListingJoin = "SELECT * FROM LISTING_CATEGORY WHERE listing_id = '".$data['id']."'";
        $resultCategoryListingJoin = mysqli_query($conn,$sqlCategoryListingJoin);
        $countCategoryListingJoin = mysqli_num_rows($resultCategoryListingJoin);
        $ListCateg = array();
        if($countCategoryListingJoin > 0){
            while ($dataCategoryListingJoin = mysqli_fetch_assoc($resultCategoryListingJoin)) {
                $ListCateg[] = $dataCategoryListingJoin['category_id'];
            }
        }else{
            $ListCateg[] = $dataListing['category_id'];
        }
        
        $data['category_id'] = $ListCateg;
    }
}

include_once('includes/header.php');
include_once('includes/security.php');
include_once('includes/sidebar.php');
include_once('includes/navbar.php');

if($id == 0) {
    echo '<h2 class="page-header">Add New</h2>';
}
else {
    echo '<h2 class="page-header">Edit</h2>';
}
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#ListeCategory').multiselect();

        $(document).on('click', 'button#UploadImgBtn', function(){
            $('input#ImgUpload').click();
        });

        $('input#ImgUpload').change(showPreviewImage_click);

        $(document).on('click', 'a.DeleteImgUpload', function(){
            $('input#CheckImgUpload').val('');
            $('button#UploadImgBtn').show();
            $(this).hide();
            $('span#image_file_info_db').attr('id', 'image_file_info');
        });
    });


    function showPreviewImage_click(e) {
        var $input = $(this);
        var inputFiles = this.files;
        if(inputFiles == undefined || inputFiles.length == 0) return;
        var inputFile = inputFiles[0];

        var reader = new FileReader();
        reader.onload = function(event) {
            $('img#ApercuImgUpload').attr("src", event.target.result);
            $('input#CheckImgUpload').val('');
            $('span#image_file_info').show();
        };
        reader.onerror = function(event) {
            alert("ERROR: " + event.target.error.code);
        };
        reader.readAsDataURL(inputFile);
    }
</script>
     
<form role="form" class="horizontal" id="edit" action="?model=<?php echo $model; ?>&action=save" method="post" enctype="multipart/form-data">
<div class="form-group col-lg-12">
<?php 
    if(!empty($erreur)){
?>
    <div class="row">
        <div class="col-lg-7">
            <span class = "error"><?php echo $erreur; ?></span>
        </div>
    </div>
<?php 
    }
?>

    <div class="row">

        <div class="col-lg-7">

            <div class = "row">
                <label class="col-sm-3 control-label" for="id">ID</label>
                <div class="col-sm-9">
<?php 
                    $ID = ($data['id'] > 0) ? $data['id'] : '0'; 
                    echo $ID;
?>
                    <input type="hidden" value="<?php echo $ID; ?>" name="id"> 
                </div>
            </div>


            <div class = "row">
                <label class="col-sm-3 control-label" for="listing_name">Listing Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" value="<?php echo $data['listing_name'] ?>" name="listing_name"> 
                </div>
            </div>


            <div class = "row">
                <label class="col-sm-3 control-label" for="description">Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description"><?php echo $data['description'] ?></textarea>
                </div>
            </div>


            <div class = "row">
                <label class="col-sm-3 control-label" for="image">Image</label>
                <div class="col-sm-9">
<?php
                    $data['image'] = trim($data['image']);
                    $classFileInfos = (!empty($data['image'])) ? 'image_file_info_db' : 'image_file_info';
?>
                    <span id="<?php echo $classFileInfos; ?>">
                        Uploaded file: 
                        <a id = "UrlImgUpload" href="#"></a>
                        <br>
                        <img id = "ApercuImgUpload" src = "<?php echo 'http://axtivo.com/graphics/'.$data['image']; ?>" width="500">
                        <br>
                        <br>
                        <input type="hidden" value="" name="image" id="image_delete_flag">
<?php 
                        if($classFileInfos == 'image_file_info_db'){
?>
                            <a class = "DeleteImgUpload" onclick="" href="#">Delete file</a>
<?php 
                        }
?>
                        <br>
                    </span>
<?php 
                    if($classFileInfos == 'image_file_info'){
?>
                        <button id = "UploadImgBtn" class="btn btn-info" type="button">Upload</button>
<?php 
                    }else{
?>
                        <button id = "UploadImgBtn" class="btn btn-info" type="button" style="display: none;">Upload</button>
<?php 
                    }
?>
                    <input id = "ImgUpload" type="file" name="image"> 
                    <input id = "CheckImgUpload" type="hidden" name="check_image" value = "<?php echo $data['image']; ?>"> 
                </div>
            </div>


            <div class = "row">
                <label class="col-sm-3 control-label" for="venue">Venue</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" value="<?php echo $data['venue'] ?>" name="venue"> 
                </div>
            </div>


            <div class = "row">
                <label class="col-sm-3 control-label" for="vacancies">Vacancies</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" value="<?php echo $data['vacancies'] ?>" name="vacancies"> 
                </div>
            </div>

<?php 
            //List All Location
            $sqlLocation = "SELECT * FROM LOCATION WHERE 1 = 1";
            $resultLocation = mysqli_query($conn,$sqlLocation);
            $countLocation = mysqli_num_rows($resultLocation);
?>

            <div class = "row">
                <label class="col-sm-3 control-label" for="location_id">Location</label>
                <div class="col-sm-9">
                    <select class="form-control" name="location_id"> 
<?php
                        while($dataLocation = mysqli_fetch_assoc($resultLocation)){
                            $selected = "";
                            if($data['location_id'] == $dataLocation['id']){
                                $selected = "selected = 'selected'";
                            }
?>
                            <option value = "<?php echo $dataLocation['id']; ?>" <?php echo $selected; ?>><?php echo $dataLocation['location_name']; ?></option>
<?php 
                        } 
?>
                    </select>
                </div>
            </div>

<?php 
            //List All Category
            $sqlCateg = "SELECT * FROM CATEGORY WHERE 1 = 1";
            $resultCateg = mysqli_query($conn,$sqlCateg);
            $countCateg = mysqli_num_rows($resultCateg);
?>

            <div class = "row">
                <label class="col-sm-3 control-label" for="category_id">Category</label>
                <div class="col-sm-9">

                    <select id="ListeCategory" multiple="multiple" name = "category_id[]">
<?php 
                        while($dataCateg = mysqli_fetch_assoc($resultCateg)){
                            $selected = "";
                            if(in_array($dataCateg['id'], $data['category_id'])){
                                $selected = "selected = 'selected'";
                            }
?>
                            <option value = "<?php echo $dataCateg['id']; ?>" <?php echo $selected; ?>><?php echo $dataCateg['category_name']; ?></option>
<?php 
                        }
?>
                    </select>

                </div>
            </div>



            <div class="row">
                <button class="btn btn-warning" type="submit">Submit</button>
            </div>
        </div>
    </div>
</div>
</form>


 <?php
include_once('includes/footer.php');
?>