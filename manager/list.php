<?php 
session_start();
if(!isset($_SESSION['axt_auth_admin'])){
    
}

include_once('includes/header.php');
include_once('includes/security.php');
include_once('includes/sidebar.php');
include_once('includes/navbar.php');

include_once('../includes/mydb.php');

$model = $_GET['model'];
$success =  (int) (isset($_GET['success'])?$_GET['success']:0);
$delsuccess =  (int) (isset($_GET['delsuccess'])?$_GET['delsuccess']:0);

$orderby = isset($_GET['orderby'])?$_GET['orderby']:'id';


$targetpage = "";
$adjacents = 3;

$limit = 25;
$page = (int) (isset($_GET['page'])?$_GET['page']:1);
if($page)
$start = ($page - 1) * $limit;          //first item to display on this page
else
$start = 0;

$ord = 0;


//$total_pages = $dm->getRowsCount();
$sqlListingCpt = "SELECT * FROM LISTING WHERE 1 = 1";
$resultListingCpt = mysqli_query($conn,$sqlListingCpt);
$countListingCpt = mysqli_num_rows($resultListingCpt);
$total_pages = $countListingCpt;

/* Setup page vars for display. */
if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
$prev = $page - 1;                          //previous page is page - 1
$next = $page + 1;                          //next page is page + 1
$lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
$lpm1 = $lastpage - 1;                      //last page minus 1

$pagination = "";
if($lastpage > 1)
{
    $pagination .= "<div class=\"pagination\"><ul class=\"pagination\">";
    //previous button
    if ($page > 1) 
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=$prev\">&laquo;</a></li>";
    else
    $pagination.= "<li class=\"disabled\"><a href=\"#\">&laquo;</a></li>";  

    //pages 
    if ($lastpage < 7 + ($adjacents * 2))   //not enough pages to bother breaking it up
    {   
    for ($counter = 1; $counter <= $lastpage; $counter++)
    {
    if ($counter == $page)
    $pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
    else
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=$counter\">$counter</a></li>";
    }
    }
    elseif($lastpage > 5 + ($adjacents * 2))    //enough pages to hide some
    {
    //close to beginning; only hide later pages
    if($page < 1 + ($adjacents * 2))        
    {
    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
    {
    if ($counter == $page)
    $pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
    else
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=$counter\">$counter</a></li>";
    }
    $pagination.= "...";
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=$lpm1\">$lpm1</a></li>";
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=$lastpage\">$lastpage</a></li>";
    }
    //in middle; hide some front and some back
    elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
    {
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=1\">1</a></li>";
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=2\">2</a></li>";
    $pagination.= "...";
    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
    {
    if ($counter == $page)
    $pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
    else
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=$counter\">$counter</a></li>";
    }
    $pagination.= "...";
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=$lpm1\">$lpm1</a></li>";
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=$lastpage\">$lastpage</a></li>";
    }
    //close to end; only hide early pages
    else
    {
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=1\">1</a></li>";
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=2\">2</a></li>";
    $pagination.= "<li><a href=\"#\">...</a></li>";
    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
    {
    if ($counter == $page)
    $pagination.= "<li class=\"active\"><a href=\"#\">$counter</a></li>";
    else
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=$counter\">$counter</a></li>";
    }
    }
    }

    //next button
    if ($page < $counter - 1) 
    $pagination.= "<li><a href=\"$targetpage?model=$model&page=$next\">&raquo;</a></li>";
    else
    $pagination.= "<li class=\"disabled\"><a href=\"#\">&raquo;</a></li>";

    $pagination.= "</ul></div>\n";      
}
?>


<h2 class="page-header">
<?php 
    echo str_replace("_"," ",$_GET['model']);
?>
</h2>

<?php if($success==1): ?>
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <i class="fa fa-check"></i> Data Saved 
</div>
<?php endif; ?>

<?php if($delsuccess==1): ?>
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <i class="fa fa-check"></i> Data Deleted 
</div>
<?php endif; ?>

<div class="table-responsive">
    <table id="list" class="table table-bordered table-hover table-striped tablesorter">
        <thead>
            <tr>
                <th class="header" scope="col">ID </th>
                <th class="header" scope="col">Listing Name </th>
                <th class="header" scope="col">Host </th>
                <th class="header" scope="col">Vacancies </th>
                <th class="header" scope="col">Location </th>
                <th class="header" scope="col">Category </th>
                <th class="header" scope="col">Actions </th>        
            </tr>
        </thead>

        <tbody>
<?php
            $sqlListing = "SELECT * FROM LISTING WHERE 1 = 1 ORDER BY id DESC LIMIT $page, $limit";
            $resultListing = mysqli_query($conn,$sqlListing);
            $countListing = mysqli_num_rows($resultListing);

            while ($dataListing = mysqli_fetch_assoc($resultListing)) {

                //Location
                $sqlLocation = "SELECT * FROM LOCATION WHERE id = '".$dataListing['location_id']."'";
                $resultLocation = mysqli_query($conn,$sqlLocation);
                $countLocation = mysqli_num_rows($resultLocation);
                $dataLocation = mysqli_fetch_assoc($resultLocation);
?>
            <tr>
                <td>
                    <?php echo $dataListing['id']; ?>
                    <input type="hidden" value="<?php echo $dataListing['id']; ?>" name="id">
                </td>

                <td><?php echo $dataListing['listing_name']; ?></td>
                <td></td>
                <td><?php echo $dataListing['vacancies']; ?></td>
                <td><?php echo $dataLocation['location_name']; ?></td>

                <td>
                    <ul>
<?php 
                    //Listing Category
                    $sqlListingCateg = "SELECT * FROM LISTING_CATEGORY WHERE listing_id = '".$dataListing['id']."'";
                    $resultListingCateg = mysqli_query($conn,$sqlListingCateg);
                    $countListingCateg = mysqli_num_rows($resultListingCateg);
                    if($countListingCateg > 0){
                        while($dataListingCateg = mysqli_fetch_assoc($resultListingCateg)){
                            $sqlCateg = "SELECT * FROM CATEGORY WHERE id = '".$dataListingCateg['category_id']."'";
                            $resultCateg = mysqli_query($conn,$sqlCateg);
                            $countCateg = mysqli_num_rows($resultCateg);
                            $dataCateg = mysqli_fetch_assoc($resultCateg);
?>
                            <li><?php echo $dataCateg['category_name']; ?></li>
<?php 
                        }
                    }else{
                            $sqlCateg = "SELECT * FROM CATEGORY WHERE id = '".$dataListing['category_id']."'";
                            $resultCateg = mysqli_query($conn,$sqlCateg);
                            $countCateg = mysqli_num_rows($resultCateg);
                            $dataCateg = mysqli_fetch_assoc($resultCateg);
?>
                            <li><?php echo $dataCateg['category_name']; ?></li>
<?php 
                    }

?>
                    </ul>
                </td>
                <td>
                    <a class="btn btn-warning btn-xs" href="/manager/edit.php?model=listing&amp;id=<?php echo $dataListing['id']; ?>">
                        <i class="fa fa-pencil"></i>
                    </a>
                    &nbsp;
                    <a class="btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this item?')" href="/manager/edit.php?model=listing&amp;action=del&amp;id=<?php echo $dataListing['id']; ?>">
                        <i class="fa fa-times-circle"></i>
                    </a>
                </td>
            </tr>
<?php 
            }
?>
        </tbody>

    </table>
</div>


<?php echo $pagination; ?>

<?php 
if ($_GET['model'] != "user_type" && $_GET['model'] != "messages" && $_GET['model'] != "static_text" && $_GET['model'] != "booking" && $_GET['model'] != "listing" && $_GET['model'] != "subscription"){
    echo '<div class="row">
            <a href="edit.php?model='.$model.'" class="btn btn-warning">Add New</a>
            </div>';
}

include_once('includes/footer.php');
?>