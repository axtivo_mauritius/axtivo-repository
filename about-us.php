<?php 
session_start();
include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');

$textSQL = mysqli_query($conn, "SELECT * FROM STATIC_TEXT where id=1");
$textArray = mysqli_fetch_assoc($textSQL);
?>

<script>
$(document).ready(function(){	
 
});		  
</script>

<div class="inside-page">
    <div class="container">
        
        <h1><?php echo $textArray['page_title']; ?></h1>  
        
<?php if(!empty($textArray['image'])): ?>
	<p><img src="graphics/<?php echo $textArray['image']; ?>" class="img img-responsive" /></p>
    <br />
<?php endif; ?>        
        
<?php echo htmlspecialchars_decode($textArray['content']); ?>
           
        
     </div>  
 </div>
 
 
 <?php include_once('includes/footer.php'); ?>