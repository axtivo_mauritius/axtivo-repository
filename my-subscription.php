<?php 
session_start();
 
include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');

if(!isset($_SESSION['axt_auth_user'])){
	die('You are not authorised to view this page.');
}
 
 if($_SESSION['user_type_id']!=1){ 
	die('You are not authorised to view this page.');
}

?>

<script>
$(document).ready(function(){	
 
});		  
</script>

<div class="inside-page">
    <div class="container">
        
         <h1>MY SUBSCRIPTION</h1>  
         
         <?php
		 
		  $subscriptionSQL = mysqli_query($conn, "select * from SUBSCRIPTION where user_id=".$_SESSION['user_id']." order by id desc");
		 
		 if (mysqli_num_rows($subscriptionSQL)!=0) {
			 echo '<div id="no-more-tables">
           					 <table class="col-md-12 table-bordered table-striped table-condensed cf">
									<thead class="cf">
									  <tr>
										<th>Ref #</th>
										<th>Plan </th>
										<th>Start Date</th>
										<th>Expiration</th>
										<th>Credits Quota</th>
										<th>Credits Left</th>
										<th>Status</th>
										<th>Action</th>
										
									  </tr>
									</thead>
									<tbody>';
							 $i = 0; 		
							 
							
							
							while ($subscriptionArray = mysqli_fetch_assoc($subscriptionSQL))  {
	 			
								echo '  <tr>
											<td data-title="Ref #">'.$subscriptionArray['id'].'</td>
											<td data-title="Plan">'.$subscriptionArray['subscription_name'].'</td>
											<td data-title="Start Date">'.date('d-m-Y',strtotime($subscriptionArray['start_date'])).'</td>
											<td data-title="Expiration">'.date('d-m-Y',strtotime($subscriptionArray['expiration_date'])).'</td>
											<td data-title="Credits Quota">'.$subscriptionArray['credits_purchased'].'</td>
											<td data-title="Credits Left">'.$subscriptionArray['credits_remaining'].'</td>
											<td data-title="Status">'.($subscriptionArray['expiration_date']>=date('Y-m-d')?"Active":"Expired").'</td>
								 
											<td data-title="Action">
											
				 
				<form action="subscribe.php" method="post" style="margin-right:5px">
					<input type="hidden" name="id" value="'.$subscriptionArray['id'].'">
					<input type="hidden" name="btn-action" value="renew">
					<button class="btn btn-default btn-action" type="submit"><i class="fa fa-refresh"></i> Renew</button>
				</form>
	 
											</td>
										</tr>';
										 $i++; 
									}
							echo '</tbody>
								  </table>
								  </div> 
								 ';
		 }
		 
		 else {
					echo '<div class="alert alert-warning">
           
            <h4>No Current Subscription</h4>
            <p>You do not currently have a subscription plan.</p><br />

            <p>
            	<a href="subscribe.php" class="btn btn-primary">Subscribe Now</a>
            </p>
          </div>';
		}
		 
		 
		 ?>
      	
            
         
        
     </div>  
 </div>
 
 
 <?php include_once('includes/footer.php'); ?>