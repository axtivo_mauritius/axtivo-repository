<?php
$table = 'SUBSCRIPTION';

$fields = array (
			array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'
	
	)	
	, 
	 array (
		'name' => 'user_id'
		, 'type' => 'lookup'
		, 'title' => 'User'
		//lookup field specific parameters
		, 'lookup_table' => 'USER'
		, 'key_field' => 'id'
		, 'values_field' => 'email'
		, 'css_class' => 'form-control'
	)
	, 
	array (
		'name' => 'subscription_name'
		, 'type' => 'input'
		, 'title' => 'Subscription Name'
		, 'css_class' => 'form-control'
	)
		, array (
		'name' => 'start_date'
		, 'type' => 'date'
		, 'title' => 'Start Date'
		, 'css_class' => 'form-control'
	)
	, array (
		'name' => 'expiration_date'
		, 'type' => 'date'
		, 'title' => 'Expiration Date'
		, 'css_class' => 'form-control'
	)
			, array (
		'name' => 'credits_purchased'
		, 'type' => 'input'
		, 'title' => 'Credits Purchased'
		, 'css_class' => 'form-control'
	)
		, array (
		'name' => 'credits_remaining'
		, 'type' => 'input'
		, 'title' => 'Credits Remaining'
		, 'css_class' => 'form-control'
	)
	 ,
	
 array (
		'name' => 'edit'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'edit_link' => 'manager/edit.php?model=subscription&id=*'
		, 'delete_link' => 'manager/edit.php?model=subscription&action=del&id=*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);
