<?php
$table = 'CATEGORY';

$fields = array (
	array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'
	
	)
	, array (
		'name' => 'category_name'
		, 'type' => 'input'
		, 'title' => 'Category'
		, 'css_class' => 'form-control'
	)
 
	, array (
		'name' => 'edit'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'edit_link' => 'manager/edit.php?model=category&id=*'
		, 'delete_link' => 'manager/edit.php?model=category&action=del&id=*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);
