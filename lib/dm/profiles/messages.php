<?php
$table = 'MESSAGES';

$fields = array (
			array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'
	
	)
		, array (
		'name' => 'member_id'
		, 'type' => 'lookup'
		, 'title' => 'Recipient'
		//lookup field specific parameters
		, 'lookup_table' => 'USER'
		, 'key_field' => 'id'
		, 'values_field' => 'email'
		, 'css_class' => 'form-control'
	)
	, array (
		'name' => 'subject'
		, 'type' => 'input'
		, 'title' => 'Subject'
		, 'css_class' => 'form-control'
	)
	, array (
		'name' => 'date_sent'
		, 'type' => 'date'
		, 'title' => 'Date Sent'
		, 'css_class' => 'form-control'
	)
	, array (
		'name' => 'booking_id'
		, 'type' => 'input'
		, 'title' => 'Booking ID'
		, 'css_class' => 'form-control'
	)
	
		, array (
		'name' => 'host_id'
		, 'type' => 'lookup'
		, 'title' => 'Host'
		//lookup field specific parameters
		, 'lookup_table' => 'USER'
		, 'key_field' => 'id'
		, 'values_field' => 'business_name'
		, 'css_class' => 'form-control'
	)
 		
	, 
	
 array (
		'name' => 'edit'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'edit_link' => 'manager/edit.php?model=messages&id=*'
		, 'delete_link' => 'manager/edit.php?model=messages&action=del&id=*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);
