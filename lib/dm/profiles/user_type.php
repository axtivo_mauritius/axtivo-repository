<?php
$table = 'USER_TYPE';

$fields = array (
	array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'
	
	)
	, array (
		'name' => 'type_name'
		, 'type' => 'input'
		, 'title' => ' Name'
		, 'css_class' => 'form-control'
	)
	
	, array (
		'name' => 'edit'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'edit_link' => 'manager/edit.php?model=user_type&id=*'
		, 'delete_link' => 'manager/edit.php?model=user_type&action=del&id=*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);
