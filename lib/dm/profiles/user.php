<?php
$table = 'USER';

$fields = array (
			array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'

	)
	, array (
		'name' => 'firstname'
		, 'type' => 'input'
		, 'title' => 'First Name'
		, 'css_class' => 'form-control'
	)
		, array (
		'name' => 'surname'
		, 'type' => 'input'
		, 'title' => 'Surname'
		, 'css_class' => 'form-control'
	)
			, array (
		'name' => 'email'
		, 'type' => 'input'
		, 'title' => 'Email'
		, 'css_class' => 'form-control'
	)
				, array (
		'name' => 'telephone'
		, 'type' => 'input'
		, 'title' => 'telephone'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)

	 , array (
		'name' => 'address_1'
		, 'type' => 'input'
		, 'title' => 'Address Line 1'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)
	, array (
		'name' => 'address_2'
		, 'type' => 'input'
		, 'title' => 'Address Line 2'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)

			, array (
		'name' => 'password'
		, 'type' => 'password'
		, 'title' => 'Password'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)

	, array (
		'name' => 'business_name'
		, 'type' => 'input'
		, 'title' => 'Business Name'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)
	, array (
		'name' => 'website'
		, 'type' => 'input'
		, 'title' => 'Website'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)
	, array (
		'name' => 'bank_name'
		, 'type' => 'input'
		, 'title' => 'Bank Name'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)
	, array (
		'name' => 'short_code'
		, 'type' => 'input'
		, 'title' => 'Short Code'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)

	, array (
		'name' => 'account_number'
		, 'type' => 'input'
		, 'title' => 'Account Number'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)
	, array (
		'name' => 'cost'
		, 'type' => 'input'
		, 'title' => 'Cost Per Class/Course'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)

		   , array (
		'name' => 'status'
		, 'type' => 'list'
		, 'title' => 'Status'
		, 'items' => array(
			"Active" => "Active"
			, "Suspended" => "Suspended"

		)
		, 'css_class' => 'form-control'
	)

			, array (
		'name' => 'user_type_id'
		, 'type' => 'lookup'
		, 'title' => 'User Type'
		//lookup field specific parameters
		, 'lookup_table' => 'USER_TYPE'
		, 'key_field' => 'id'
		, 'values_field' => 'type_name'
		, 'css_class' => 'form-control'
	)
	,

 array (
		'name' => 'edit'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'edit_link' => 'manager/edit.php?model=user&id=*'
		, 'delete_link' => 'manager/edit.php?model=user&action=del&id=*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);
