<?php
$table = 'LOCATION';

$fields = array (
	array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'
	
	)
	, array (
		'name' => 'location_name'
		, 'type' => 'input'
		, 'title' => 'Location Name'
		, 'css_class' => 'form-control'
	)
 
	, array (
		'name' => 'edit'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'edit_link' => 'manager/edit.php?model=location&id=*'
		, 'delete_link' => 'manager/edit.php?model=location&action=del&id=*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);
