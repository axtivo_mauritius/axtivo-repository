<?php
$table = 'ADMIN_STAFF';

$fields = array (
			array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'
	
	)	
	, array (
		'name' => 'first_name'
		, 'type' => 'input'
		, 'title' => 'First Name'
		, 'css_class' => 'form-control'
	)
		, array (
		'name' => 'last_name'
		, 'type' => 'input'
		, 'title' => 'Surname'
		, 'css_class' => 'form-control'
	)
			, array (
		'name' => 'email'
		, 'type' => 'input'
		, 'title' => 'Email'
		, 'css_class' => 'form-control'
	)
	 
			, array (
		'name' => 'password'
		, 'type' => 'password'
		, 'title' => 'Password'
		, 'css_class' => 'form-control'
		, 'list_mode' => 0
	)
	
 
	
		   , array (
		'name' => 'status'
		, 'type' => 'list'
		, 'title' => 'Status'
		, 'items' => array(
			"Active" => "Active"
			, "Suspended" => "Suspended"
			
		)
		, 'css_class' => 'form-control'
	) 
	, 
	
 array (
		'name' => 'edit'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'edit_link' => 'manager/edit.php?model=admin_staff&id=*'
		, 'delete_link' => 'manager/edit.php?model=admin_staff&action=del&id=*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);
