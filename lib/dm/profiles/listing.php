<?php
$table = 'LISTING';

$fields = array (
			array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'
		, 'css_class' => 'id'


	)
	, array (
		'name' => 'listing_name'
		, 'type' => 'input'
		, 'title' => 'Listing Name'
		, 'css_class' => 'form-control'
	)
		,
		 array (
		'name' => 'user_id'
		, 'type' => 'lookup'
		, 'title' => 'Host'
		//lookup field specific parameters
		, 'lookup_table' => 'USER'
		, 'key_field' => 'id'
		, 'values_field' => 'business_name'
		, 'css_class' => 'form-control'
		, 'single_mode' => 0
	)
	,
	 array (
		'name' => 'description'
		, 'type' => 'textarea'
		, 'title' => 'Description'
		, 'css_class' => 'form-control'
		,'list_mode' => 0
	)
		, array (
		'name' => 'image'
		, 'type' => 'file_upload'
		, 'title' => 'Image'
		, 'key_field' => 'id'
		, 'image_url' => 'http://axtivo.com/graphics/'
		, 'download_url' => 'http://axtivo.com/graphics/'
		, 'upload_path' => 'http://axtivo.com/graphics/'
		,'max_filesize_mb' => 1
		,'allowed_ext' => 'jpg'
		,'is_delete_option' => 1
		//,'is_delete_from_disk'=> 1
		, 'css_class' => 'form-control'
		,'list_mode' => 0
	)
	
		, array (
		'name' => 'venue'
		, 'type' => 'input'
		, 'title' => 'Venue'
		, 'css_class' => 'form-control'
		,'list_mode' => 0
	)
	, array (
		'name' => 'vacancies'
		, 'type' => 'input'
		, 'title' => 'Vacancies'
		, 'css_class' => 'form-control'
	)

			, array (
		'name' => 'location_id'
		, 'type' => 'lookup'
		, 'title' => 'Location'
		//lookup field specific parameters
		, 'lookup_table' => 'LOCATION'
		, 'key_field' => 'id'
		, 'values_field' => 'location_name'
		, 'css_class' => 'form-control'
	)
	,


		 array (
		'name' => 'category_id'
		, 'type' => 'lookup'
		, 'title' => 'Category'
		//lookup field specific parameters
		, 'lookup_table' => 'CATEGORY'
		, 'key_field' => 'id'
		, 'values_field' => 'category_name'
		, 'css_class' => 'form-control'
	)
	,

 array (
		'name' => 'edit'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'edit_link' => 'manager/edit.php?model=listing&id=*'
		, 'delete_link' => 'manager/edit.php?model=listing&action=del&id=*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);
