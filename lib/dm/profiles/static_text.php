<?php
$table = 'STATIC_TEXT';

$fields = array (
	array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'
	
	)
	, array (
		'name' => 'page_title'
		, 'type' => 'input'
		, 'title' => ' Page Title'
		, 'css_class' => 'form-control'
	)
	, array (
		'name' => 'content'
		, 'type' => 'wysiwyg'
		, 'title' => 'Content'
		, 'css_class' => 'form-control'
		, 
		'list_mode' => 0
	)
	
		, array (
		'name' => 'image'
		, 'type' => 'file_upload'
		, 'title' => 'Image'
		, 'key_field' => 'id'
		, 'image_url' => 'http://axtivo.com/graphics/'
		, 'download_url' => 'http://axtivo.com/graphics/'
		, 'upload_path' => 'http://axtivo.com/graphics/'
		,'max_filesize_mb' => 1
		,'allowed_ext' => 'jpg'
		,'is_delete_option' => 1
		//,'is_delete_from_disk'=> 1
		, 'css_class' => 'form-control'
		,'list_mode' => 0
	)
		
	, array (
		'name' => 'edit'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'edit_link' => 'manager/edit.php?model=static_text&id=*'
		 , 'delete_link' => 'manager/edit.php?model=static_text&action=del&id=*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);
