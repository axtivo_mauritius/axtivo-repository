<?php
$table = 'BOOKING';

$fields = array (
			array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'
	
	)	

		
			, array (
		'name' => 'listing_id'
		, 'type' => 'lookup'
		, 'title' => 'Listing'
		//lookup field specific parameters
		, 'lookup_table' => 'LISTING'
		, 'key_field' => 'id'
		, 'values_field' => 'listing_name'
		, 'css_class' => 'form-control'
	)
	,

		 array (
		'name' => 'user_id'
		, 'type' => 'lookup'
		, 'title' => 'User'
		//lookup field specific parameters
		, 'lookup_table' => 'USER'
		, 'key_field' => 'id'
		, 'values_field' => 'email'
		, 'css_class' => 'form-control'
	)
 ,
  array (
		'name' => 'host_id'
		, 'type' => 'lookup'
		, 'title' => 'Host'
		//lookup field specific parameters
		, 'lookup_table' => 'USER'
		, 'key_field' => 'id'
		, 'values_field' => 'business_name'
		, 'css_class' => 'form-control'
	)
 ,
 
 	array (
		'name' => 'booking_date'
		, 'type' => 'date'
		, 'title' => 'Booking Date'
		, 'css_class' => 'form-control'
	)
	,
	
	
 array (
		'name' => 'edit'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'edit_link' => 'manager/edit.php?model=booking&id=*'
		, 'delete_link' => 'manager/edit.php?model=booking&action=del&id=*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);
