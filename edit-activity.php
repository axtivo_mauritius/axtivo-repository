<?php session_start();

if(!isset($_SESSION['axt_auth_user']) || $_SESSION['user_type_id']!=2){
	die('Access Denied.');
}

include_once('includes/mydb.php');

include_once 'lib/dm/DataManager.php';

$model = "listing";
$id = isset($_POST['id'])?$_POST['id']:0;


		if(!$_POST){
			die('Action denied.');
		}

		$data = $_POST;

		$data['user_id']=$_SESSION['user_id'];

		$listingSQL = mysqli_query($conn, "SELECT * FROM LISTING where id=".$id);
		$listingArray = mysqli_fetch_assoc($listingSQL);

		$listingMetaSQL = mysqli_query($conn, "SELECT * FROM LISTING_META where listing_id=".$id." LIMIT 1");
		$listingMetaArray = mysqli_fetch_assoc($listingMetaSQL);

		$categorySQL = mysqli_query($conn, "SELECT * FROM CATEGORY");

		$locationSQL = mysqli_query($conn, "SELECT * FROM LOCATION");

		//header('location: my-activities.php?success');
		//exit;


include_once('includes/header.php');
include_once('includes/menu.php');
?>


<script>
$(document).ready(function(){

	$( "#inputStartsOn" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
		$("#inputStartsOn").valid();
	} });


	$( "#inputEndsOn" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
		$("#inputEndsOn").valid();
	} });

	$('.time').timepicker({
		'scrollDefault': 'now',
		'showDuration': true,
		'timeFormat': 'H:i',
		'step': 15
	});

	var time = document.getElementById('inputTime');
	var timeDatepair = new Datepair(time);

	$('#repeatOnEveryBox').hide();
	$('#repeatEveryBox').hide();
	$('#EndsOnBox').hide();
	$('#startOnLabel').text('Date: ');


	var checkRepeat = $('#selectRepeat').val();
	hide(checkRepeat);

	$('#selectRepeat').change(function(){
		var repeatType = $(this).val();
		hide(repeatType);
	});

	function hide(repeatType){
		if (repeatType == "daily")
		{
			$('#repeatOnEveryBox').hide();
			$('#repeatEveryBox').show();
			$('#repeatText').text("days");
			$('#startOnLabel').text('Starts On: ');
			$('#EndsOnBox').show();
		}
		else if ( repeatType == "weekly"){
			$('#repeatOnEveryBox').show();
			$('#repeatEveryBox').show();
			$('#repeatText').text("weeks");
			$('#startOnLabel').text('Starts On: ');
			$('#EndsOnBox').show();
		}
		else if ( repeatType == "monthly"){
			$('#repeatOnEveryBox').hide();
			$('#repeatText').text("months");
			$('#EndsOnBox').show();
			$('#startOnLabel').text('Date: ');
		}
		else if (repeatType == "once"){
			$('#repeatOnEveryBox').hide();
			$('#repeatEveryBox').hide();
			$('#EndsOnBox').hide();
			$('#startOnLabel').text('Date: ');
		}
	}


});
</script>

<div class="inside-page">
	<div class="container">
		<h1>Edit Activity</h1>

		<form role="form" class="horizontal col-md-6" id="create-activity" action="edit-activity-schedule.php?action=edit" method="post" enctype="multipart/form-data">
			<?php echo'<input type="hidden"  name="id" value="'.$id.'"  />'; ?>

			<dl class="dl-horizontal">
				<dt><label for="listing_name">Listing Name</label></dt>
				<dd>
					<?php echo '<input type="text"  name="listing_name"  value="'.$listingArray['listing_name'].'" class="form-control" />'; ?>
					&nbsp;
				</dd>
					<dd></dd>

				</dl>


				<dl class="dl-horizontal">
					<dt><label for="description">Description</label></dt>
					<dd>
						<?php echo '<textarea  name="description"  class="form-control"  >'.$listingArray['description'].'</textarea>'; ?>
						&nbsp;</dd>
						<dd></dd>

					</dl>




					<dl class="dl-horizontal">
						<dt><label for="image">Image</label></dt>
						<dd>
							<?php
							$default_pic = "images/no-image.png";
							$check_pic = "graphics/".$listingArray['image'];
							if (empty($listingArray['image'])) {
								$display_pic = "<img src='".$default_pic."' class='img img-responsive' />";
							} elseif (file_exists($check_pic)){
								$display_pic = "<img src='".$check_pic."' class='img img-responsive'  />";
							}
							else {
								$display_pic = "<img src='".$default_pic."' class='img img-responsive'  />";
							}
							echo $display_pic;


							 ?>
							<input type="file"  name="image"  />
										<?php echo'<input type="hidden"  name="imageFileName" value="'.$listingArray['image'].'"  />'; ?>
							&nbsp;</dd>
							<dd></dd>

						</dl>

						<dl class="dl-horizontal">
							<dt><label for="category_id">Category</label></dt>
							<dd><select  name="category_id" class="form-control"  />

								<?php
										while ($categoryArray = mysqli_fetch_assoc($categorySQL))
										{
											if ($listingArray['category_id'] == $categoryArray['id'])
											{
													echo '<option value="'.$categoryArray['id'].'" selected>'.$categoryArray['category_name'].'</option>';
											}
											else {
													echo '<option value="'.$categoryArray['id'].'">'.$categoryArray['category_name'].'</option>';
											}

										}

								?>

							</select>
							&nbsp;</dd>
							<dd></dd>

						</dl>

						<div id="StartsOnBox">
							<dl class="dl-horizontal">
								<dt><label for="inputStartsOn" id="startOnLabel">Date: </label></dt>
								<dd>
									<?php echo '<input class="form-control" id="inputStartsOn" name="inputStartsOn" type="text" value="'.date('d-m-Y',strtotime($listingMetaArray['repeat_start'])).'"/>';?>

									&nbsp;</dd>

								</dl>
							</div>

							<div id="inputTime">
								<dl class="dl-horizontal">
									<dt><label for="inputTimeStart" >Start Time: </label></dt>
									<dd>
										<?php echo '<input class="form-control time start ui-timepicker-input" id="inputTimeStart" name="inputTimeStart" type="text" value="'.$listingMetaArray['start_time'].'"/>'; ?>
										&nbsp;</dd>

									</dl>


									<dl class="dl-horizontal">
										<dt><label for="inputTimeEnd">End Time: </label></dt>
										<dd>

											<?php echo '<input class="form-control time end ui-timepicker-input" id="inputTimeEnd" name="inputTimeEnd" type="text" value="'.$listingMetaArray['end_time'].'"/>'; ?>
											&nbsp;</dd>

										</dl>
									</div>


									<dl class="dl-horizontal">
										<dt><label for="selectRepeat">Repeats: </label></dt>
										<dd>
											<select class="form-control" name="selectRepeat" id="selectRepeat" required>
												<?php
													if ($listingMetaArray['type'] == 'Once'){
														echo '<option value="once" selected>No</option>
																	<option value="weekly">Yes</option>';
													}
													elseif ($listingMetaArray['type'] == 'Weekly')
													{
														echo '<option value="once">No</option>
																	<option value="weekly" selected>Yes</option>';
													}
													else {
														echo '<option value="once">No</option>
																	<option value="weekly">Yes</option>';
													}
												?>

											</select>
										</dd>
									</dl>


<div id="repeatEveryBox">
									<dl class="dl-horizontal">
										<dt><label for="selectRepeatEvery">Repeat Every: </label></dt>
										<dd>
											<select class="form-control" id="selectRepeatEvery" name="selectRepeatEvery" required>
												<?php
														$repeatInv = $listingMetaArray['repeat_interval'];

														for ($i = 1; $i<=20; $i++)
														{
															if ($i == 1 && ((int)$repeatInv/7) == $i){
																echo '<option value="1" selected>1 week</option>';
															}
															elseif ($i == 1 ) {
																echo '<option value="1">1 week</option>';
															}
															elseif ((int)$repeatInv/7 == $i) {
																echo '<option value="'.$i.'" selected>'.$i.' weeks</option>';
															}
															else {
																	echo '<option value="'.$i.'" >'.$i.' weeks</option>';
															}

														}

												?>



											</select>
										</dd>
									</dl>
								</div>

								<div id="repeatOnEveryBox">

									<dl class="dl-horizontal">
										<dt><label for="selectRepeatOn">Repeat on: </label></dt>
										<dd>
											<div class="checkbox">
											<?php
												$weekDaysArray = explode(",", $listingMetaArray[weekly_days]);
												for ($i = 0; $i<7; $i++)
												{
													echo "<label>";

														if (in_array($i, $weekDaysArray) && $i == 0 && $weekDaysArray.length != 0)
														{
																echo '<input name="inputRepeatOn[]" value="0" type="checkbox" checked> S 	&nbsp;';
														}
														elseif ($i == 0){
																echo '<input name="inputRepeatOn[]" value="0" type="checkbox"> S 	&nbsp;';
														}

														if (in_array($i, $weekDaysArray) && $i == 1)
														{
																echo '<input name="inputRepeatOn[]" value="1" type="checkbox" checked> M 	&nbsp;';
														}
														elseif ($i == 1){
																echo '<input name="inputRepeatOn[]" value="1" type="checkbox"> M 	&nbsp;';
														}

														if (in_array($i, $weekDaysArray) && $i == 2)
														{
																echo '<input name="inputRepeatOn[]" value="2" type="checkbox" checked> T 	&nbsp;';
														}
														elseif ($i == 2){
																echo '<input name="inputRepeatOn[]" value="2" type="checkbox"> T 	&nbsp;';
														}

														if (in_array($i, $weekDaysArray) && $i == 3)
														{
																echo '<input name="inputRepeatOn[]" value="3" type="checkbox" checked> W 	&nbsp;';
														}
														elseif ($i == 3){
																echo '<input name="inputRepeatOn[]" value="3" type="checkbox"> W 	&nbsp;';
														}

														if (in_array($i, $weekDaysArray) && $i == 4)
														{
																echo '<input name="inputRepeatOn[]" value="4" type="checkbox" checked> T 	&nbsp;';
														}
														elseif ($i == 4){
																echo '<input name="inputRepeatOn[]" value="4" type="checkbox"> T 	&nbsp;';
														}

														if (in_array($i, $weekDaysArray) && $i == 5)
														{
																echo '<input name="inputRepeatOn[]" value="5" type="checkbox" checked> F 	&nbsp;';
														}
														elseif ($i == 5){
																echo '<input name="inputRepeatOn[]" value="5" type="checkbox"> F 	&nbsp;';
														}

														if (in_array($i, $weekDaysArray) && $i == 6)
														{
																echo '<input name="inputRepeatOn[]" value="6" type="checkbox" checked> S 	&nbsp;';
														}
														elseif ($i == 6){
																echo '<input name="inputRepeatOn[]" value="6" type="checkbox"> S 	&nbsp;';
														}
														echo "	</label>";

												}



											 ?>

											</div>

										</dd>
									</dl>
								</div>



									<dl class="dl-horizontal">
										<dt><label for="vacancies">Vacancies</label></dt>
										<dd>
											<?php echo '<input type="number"  name="vacancies"  class="form-control" value="'.$listingArray[vacancies] .'" />'; ?>
											&nbsp;</dd>
											<dd></dd>

										</dl>

										<dl class="dl-horizontal">
											<dt><label for="location_id">Location</label></dt>
											<dd><select  name="location_id" class="form-control"  />

												<?php
														while ($locationArray = mysqli_fetch_assoc($locationSQL))
														{
															if ($listingArray['location_id'] == $locationArray['id'])
															{
																	echo '<option value="'.$locationArray['id'].'" selected>'.$locationArray['location_name'].'</option>';
															}
															else {
																	echo '<option value="'.$locationArray['id'].'">'.$locationArray['location_name'].'</option>';
															}

														}

												?>

											</select>
											&nbsp;</dd>
											<dd></dd>

										</dl>


										<dl class="dl-horizontal">
											<dt><label for="venue">Venue</label></dt>
											<dd>
											<?php echo '<input type="text"  name="venue"  class="form-control" value="'.$listingArray[venue].'" />'; ?>
												&nbsp;</dd>
												<dd></dd>

											</dl>


											<div class="form-group" id="EndsOnBox">
												<dl class="dl-horizontal">
													<dt><label for="inputEndsOn">End Date</label></dt>
													<dd>
														<?php
														$e_date = empty($listingMetaArray['repeat_end']) ? '' : date('d-m-Y',strtotime($listingMetaArray['repeat_end']));
														echo '<input class="form-control" id="inputEndsOn" name="inputEndsOn" type="text" value="'.$e_date.'"/>';
														?>
														Only add an end date when your activity has a fixed ending scheduled
													</dd>

												</dl>
											</div>


											<div class="row">
												<button type="submit" class="btn btn-primary btn-lg" >Submit</button>
											</div>


										</form>

									</div>
								</div>


								<?php include_once('includes/footer.php'); ?>
