<?php
session_start();

if($_POST){
	$by_location = $_POST['location'];
    $by_activity = $_POST['activity'];
    $by_date_from = $_POST['date_from'];
    $by_date_to = $_POST['date_to'];

  	$query = "SELECT LISTING.id, LISTING.image, LISTING.listing_name, LISTING.description, LISTING_META.repeat_start, LISTING_META.type FROM LISTING INNER JOIN LISTING_META ON LISTING.id = LISTING_META.listing_id";
		$conditions = array();

		if($by_location !="") {
			$conditions[] = "LISTING.location_id='$by_location'";
		}
		if($by_activity !="") {
			$conditions[] = "LISTING.category_id='$by_activity'";
		}
		if($by_date_from !="") {
			$conditions[] = "((LISTING_META.repeat_start>='".date('Y-m-d',strtotime($by_date_from))."' AND LISTING_META.type='Once') OR (LISTING_META.repeat_start<= ".date('Y-m-d',strtotime($by_date_from))." AND LISTING_META.type!='Once' ))";
		}

		$sql = $query;
		if (count($conditions) > 0) {
			$sql .= " WHERE " . implode(' AND ', $conditions);
		}

		if($by_date_to !="") {
			$sql = $sql ." OR ((LISTING_META.repeat_end >= ".date('Y-m-d',strtotime($by_date_to))." AND LISTING_META.type!='Once' ))";
		}

	 $listingSQL = mysqli_query($conn,$sql);

}
else{
	$listingSQL = mysqli_query($conn, "SELECT LISTING.id, LISTING.image, LISTING.listing_name, LISTING.description, LISTING_META.repeat_start, LISTING_META.type FROM LISTING JOIN LISTING_META ON LISTING.id = LISTING_META.listing_id where (LISTING_META.repeat_start<=CURDATE() AND LISTING_META.repeat_end >= CURDATE() OR LISTING_META.repeat_end IS NULL AND LISTING_META.repeat_interval IS NOT NULL AND LISTING_META.type!='Once') OR (LISTING_META.repeat_start>=CURDATE() AND LISTING_META.type='Once')");

}

echo '<div class="row" id="activity-item">';

if(mysqli_num_rows($listingSQL)>0){

while ($listingArray = mysqli_fetch_assoc($listingSQL)) {

	//$locationSQL = mysqli_query($conn, "SELECT * FROM LOCATION where id=".$listingArray['location_id']);
	//$locationArray=mysqli_fetch_assoc($locationSQL);

		echo '<div class="col-md-4" id="listing-panel" >
				<div class="panel panel-default">';

				$default_pic = "images/no-image.png";
				$check_pic = "graphics/".$listingArray['image'];
				if (empty($listingArray['image'])) {
					$display_pic = "<img src='".$default_pic."' class='img img-responsive' style='max-height:250px' />";
				} elseif (file_exists($check_pic)){
					$display_pic = "<img src='".$check_pic."' class='img img-responsive' style='max-height:250px' />";
				}
				else {
					$display_pic = "<img src='".$default_pic."' class='img img-responsive' style='max-height:250px' />";
				}
				echo $display_pic;

		echo'<div class="panel-body">
					<h2>'.$listingArray['listing_name'].'	</h2>
					<div class="info">';

		if ($listingArray['type'] == 'Once')
		{
				echo '<span class="activity-date"><i class="fa fa-calendar"></i> '.date('d-m-Y',strtotime($listingArray['repeat_start'])).'</span>';
		}

		echo '</div>


					<p class="description" style="padding-top:10px; padding-bottom:10px;">'.$listingArray['description'].'</p>
					<form action="view-activity.php" method="post" >
					<input type="hidden" name="id" value="'.$listingArray['id'].'">
					<button class="btn btn-primary btn-lg" role="button" type="submit">
						<span>VIEW</span>
						<i class="fa fa-angle-right"></i>
						</button>
						</form>
				  </div>
				</div>
			  </div>
				<div class="clear"></div>
			 ';

 }
}
else{
	echo '<div class="alert alert-warning"> No results found.</div>';
}

 echo '</div>';
 ?>
