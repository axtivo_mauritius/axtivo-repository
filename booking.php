<?php 
session_start();

if(!isset($_SESSION['axt_auth_user'])){
	header('Location: register.php?message');
	exit;
	
}

if($_SESSION['user_type_id']!=1){ 
	die('You are not authorised to view this page.');
}
 
include_once('includes/mydb.php');


$listingSQL = mysqli_query($conn, "SELECT * FROM LISTING where id=".$_POST['id']);
$listingArray = mysqli_fetch_assoc($listingSQL);

$userSQL = mysqli_query($conn, "SELECT * FROM USER where id=".$listingArray['user_id']);
$userArray = mysqli_fetch_assoc($userSQL);

$categorySQL = mysqli_query($conn, "SELECT * FROM CATEGORY where id=".$listingArray['category_id']);
$categoryArray = mysqli_fetch_assoc($categorySQL);

$locationSQL = mysqli_query($conn, "SELECT * FROM LOCATION where id=".$listingArray['location_id']);
$locationArray = mysqli_fetch_assoc($locationSQL);

//Check if customer has a valid subscription
$subcriptionSQL = mysqli_query($conn, "SELECT * FROM SUBSCRIPTION where expiration_date>=".date('Y-m-d')." and user_id=".$_SESSION['user_id']);
$subcriptionArray = mysqli_fetch_assoc($subcriptionSQL);

if(mysqli_num_rows($subcriptionSQL)==1){
	$activeSubscription = true;	
}
else{
	$activeSubscription = false;	
}

//Check if customer has enough credits
if($subcriptionArray['credits_remaining']>0){
	$hasCredits = true;
}
else{
	$hasCredits = false;
}

//Check if customer has booked this before
$bookingSQL = mysqli_query($conn, "select * from BOOKING where user_id=".$_SESSION['user_id']." and listing_id=".$listingArray['id']);

if(mysqli_num_rows($bookingSQL)==1){
	$bookedBefore = true;	
}
else{
	$bookedBefore = false;	
}

if($activeSubscription==true && $hasCredits==true && $bookedBefore==false){

//Save booking in database
					include_once 'lib/dm/DataManager.php';
				 
					$dm = new DataManager("booking");
					
					$dm->setMode('single');
					
					if ($dm->loadData() === false) {
							var_dump($dm->getErrors());
							exit;
					}
					
				   $data = array();
 
				   $data["listing_id"] = $listingArray['id'];
				   $data["user_id"] = $_SESSION['user_id'];
				   $data["host_id"] = $listingArray['user_id'];
				   $data["booking_date"] = date('Y-m-d');

					if ($dm->save($data) === false) {
							var_dump($dm->getErrors());
							exit;
					}
					
					 $booking_id = $dm->getKeyValue();
 

//Update credit count
$subcriptionArray['credits_remaining'] = $subcriptionArray['credits_remaining'] - 1;
$updateCredits = mysqli_query($conn,"UPDATE SUBSCRIPTION SET credits_remaining=".$subcriptionArray['credits_remaining']." WHERE user_id=".$_SESSION['user_id']);

//update vacancies
$listingArray['vacancies']=$listingArray['vacancies'] - 1;
$updateVacancies = mysqli_query($conn,"UPDATE LISTING SET vacancies=".$listingArray['vacancies']." WHERE id=".$listingArray['id']);

//Send Email

$customerSQL = mysqli_query($conn, "SELECT * FROM USER where id=".$_SESSION['user_id']);
$customerArray = mysqli_fetch_assoc($customerSQL);
 
$title = "New Booking";
$content = ' 
			<table width="100%" border="0" cellpadding="0" id="order">
				  <tr style="background:#f2f2f2;">
					<td width="30%"><strong>Customer Name</strong></td>
					<td>'.$customerArray['firstname'].' '.$customerArray['surname'].'</td>
				  </tr>
				  <tr>
					<td><strong>Activity Booked </strong></td>
					<td>'.$listingArray['listing_name'].'</td>
				  </tr>
				 <tr style="background:#f2f2f2;">
					<td><strong>Date </strong></td>
					<td>'.date('d-m-Y',strtotime($listingArray['start_date'])).'</td>
				  </tr>
				  <tr>
					<td><strong>Time</strong></td>
					<td>'
						.$listingArray['time'].
					'</td>
				  </tr>
				  
				  <tr style="background:#f2f2f2;">
					<td><strong>Venue</strong></td>
					<td>'.$listingArray['venue'].'</td>
				  </tr>
				 
				</table>
			 ';

// Retrieve the email template 
$message = file_get_contents('schedule_tmpl/email-template.html'); 

$message = str_replace('%title%', $title, $message);
$message = str_replace('%content%', $content, $message);

require 'lib/phpmailer/class.phpmailer.php';

$mail = new PHPMailer;

$mail->From = 'info@axtivo.com';
$mail->FromName = 'Axtivo';

$mail->addAddress($customerArray['email'], $customerArray['firstname'].' '.$customerArray['surname']);  
$mail->addAddress($userArray['email'], $userArray['business_name']);  

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'New Booking';
$mail->Body    = $message;

if(!$mail->send()) {
  $emailError = true;
   //exit;
}

//Save message in database
 
					$dm2 = new DataManager("messages");
					
					$dm2->setMode('single');
					
					if ($dm2->loadData() === false) {
							var_dump($dm2->getErrors());
							exit;
					}
					
				   $dataMSG = array();
 
 				   $dataMSG["subject"] = "New Booking";
				   $dataMSG["date_sent"] = date('Y-m-d');
				   $dataMSG["booking_id"] = $booking_id;
				   $dataMSG["host_id"] = $listingArray['user_id'];
				   $dataMSG["member_id"] = $_SESSION['user_id'];
				   
				   
				   
				   
				   

					if ($dm2->save($dataMSG) === false) {
							var_dump($dm2->getErrors());
							exit;
					}
}




include_once('includes/header.php');
include_once('includes/menu.php');

?>

<script>
$(document).ready(function(){	
 
});		  
</script>

<div class="inside-page">
    <div class="container">
        
         <h1>BOOKING</h1>  
         
         <?php if($activeSubscription==true && $hasCredits==true && $bookedBefore==false): ?>
         		
                <div class="alert alert-success">
            	<p>Your booking confirmation has been sent to your email. </p>
             </div>
                  
                                  <a href="my-bookings.php" class="btn btn-default btn-lg">My Bookings</a> 
         
         <?php elseif($bookedBefore==true): ?>
         
         	<div class="alert alert-danger">
            	<p>You have already booked this activity once this month. </p>
             </div>
             
         <?php elseif($emailError==true): 
             
              echo '<div class="alert alert-dangert">Mail Error: ' . $mail->ErrorInfo.'</div>';
			  
			  ?>
         
         <?php else: ?>
         
      		<div class="alert alert-warning">
            	<p>You need to login or register for an account to be able to book an activity. </p>
             </div>
                   <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">LOGIN</button>
                                  <a href="register.php" class="btn btn-default btn-lg">REGISTER</a> 
        <?php endif; ?>    
        
        
     </div>  
 </div>
 
 
 <?php include_once('includes/footer.php'); ?>