<?php 
session_start();

if(!isset($_SESSION['axt_auth_user'])){
	die('You are not authorised to view this page.');
}
 
include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');
 
 
?>

<script>
$(document).ready(function(){	
 
});		  
</script>

<div class="inside-page">
    <div class="container">
        
         <h1>MY BOOKINGS</h1>  
         
         <?php
		 
		 if($_SESSION['user_type_id']==2){ 
		 	$bookingSQL = mysqli_query($conn, "select * from BOOKING where host_id=".$_SESSION['user_id']." order by id desc");
		 }
		 else{
			 $bookingSQL = mysqli_query($conn, "select * from BOOKING where user_id=".$_SESSION['user_id']." order by id desc");
		 }
		 
		 if (mysqli_num_rows($bookingSQL)!=0) {
			 echo '<div id="no-more-tables">
           					 <table class="col-md-12 table-bordered table-striped table-condensed cf">
									<thead class="cf">
									  <tr>
										<th>#</th>
										<th>Activity </th>
										<th>Date</th>
										<th>Time</th>
										<th>'.($_SESSION['user_type_id']==2?"Member":"Host").'</th>
										<th>Action</th>
										
									  </tr>
									</thead>
									<tbody>';
							 $i = 0; 		
							
							while ($bookingArray = mysqli_fetch_assoc($bookingSQL))  {
 
							$listingSQL = mysqli_query($conn, "SELECT * FROM LISTING where id=".$bookingArray['listing_id']);
							if(mysqli_num_rows($listingSQL)>0){
								$listingArray = mysqli_fetch_assoc($listingSQL);
							}
							
							$userSQL = mysqli_query($conn, "SELECT * FROM USER where id=".$bookingArray['host_id']);
							if(mysqli_num_rows($userSQL)>0){
								$userArray = mysqli_fetch_assoc($userSQL);
							}
							
							if($_SESSION['user_type_id']==2){
									$customerSQL = mysqli_query($conn, "SELECT * FROM USER where id=".$bookingArray['user_id']);
									if(mysqli_num_rows($customerSQL)>0){
											$customerArray = mysqli_fetch_assoc($customerSQL);	
									}
							}
	
 								
								echo '  <tr>
											<td data-title="#">'.$bookingArray['id'].'</td>
											<td data-title="Activity">'.$listingArray['listing_name'].'</td>
											<td data-title="Date">'.date('d-m-Y',strtotime($listingArray['start_date'])).'</td>
											<td data-title="Time">'.date('H:i',strtotime($listingArray['time'])).'</td>
											<td data-title="'.($_SESSION['user_type_id']==2?"Member":"Host").'">'.($_SESSION['user_type_id']==2?$customerArray['firstname']." ".$customerArray['surname']:$userArray['business_name']).'</td>
										 

											 
											
											<td data-title="Action">
	
				<form action="view-activity.php" method="post" style="margin-right:5px">
					<input type="hidden" name="id" value="'.$listingArray['id'].'">
					<input type="hidden" name="btn-action" value="view">
					<button class="btn btn-default btn-action" type="submit"><i class="fa fa-eye"></i> View Activity</button>
				</form>
				
				 
											</td>
										</tr>';
										 $i++; 
									}
							echo '</tbody>
								  </table>
								  </div> 
								 ';
		 }
		 
		 else {
						echo '<div class="alert alert-warning">
           
            <h4>No Bookings Found</h4>
           ';
				if($_SESSION['user_type_id']==1){ 
				   echo ' <p>You can book an activity by pressing on the button below. </p><br />
				    <p>
					<a href="activities.php" class="btn btn-primary">Book New</a>
					</p>';
				}
					
        echo '  </div>';
						}
		 
		 
		 ?>
      	
            
         
        
     </div>  
 </div>
 
 
 <?php include_once('includes/footer.php'); ?>