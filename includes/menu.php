  <nav class="navbar navbar-default hidden-lg hidden-md" role="navigation">
                      <div class="container">
                      
                      <!-- Mobile Header -->
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                             <?php 
								 if (isset($_SESSION['axt_auth_user']) && $_SESSION['status']=="Active"):
								 ?> 
                                 <i class="fa fa-user"></i>
                                 <?php else: ?>
                            <i class="fa fa-lock"></i>
                            <?php endif; ?>
                          </button>
                         <a href="index.php"> <img alt="Axtivo" src="images/mobile-logo.png" /></a>
                        </div>
                        
                      <!-- Mobile Partner Menu -->  
                        
                        <div id="navbar" class="navbar-collapse collapse">
                        
                        <?php 
								 if (isset($_SESSION['axt_auth_user']) && $_SESSION['status']=="Active"):
								 ?>  
									
									 
									  <ul class="list-group" id="partner-menu">
                                      
                                         <li  class="list-group-item"><a href="user-dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                      
                                      <?php if($_SESSION['user_type_id']==1):?>
										 <li  class="list-group-item"><a href="my-bookings.php"><i class="fa fa-calendar"></i> My Bookings</a></li>
										 <li  class="list-group-item"><a href="my-messages.php"><i class="fa fa-envelope"></i> My Messages</a></li>
                                         <li  class="list-group-item"><a href="my-subscription.php"><i class="fa fa-credit-card"></i> My Subscription</a></li>
                  					<?php endif; ?>
                                    
                                     <?php if($_SESSION['user_type_id']==2):?>
                                     	 <li  class="list-group-item"><a href="my-bookings.php"><i class="fa fa-calendar"></i> My Bookings</a></li>
                                         <li  class="list-group-item"><a href="my-activities.php"><i class="fa fa-file-text"></i> My Activities</a></li>
										 <li  class="list-group-item"><a href="my-messages.php"><i class="fa fa-envelope"></i> My Messages</a></li>
                                     <?php endif; ?>
                                     
										 
										 <li  class="list-group-item"><a href="requests/logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                                      
                                 
									  </ul>
									
					
									<?php
									else:
								?>
                                
								<!-- Mobile Partner Login -->  
                                
									<form class="navbar-form navbar-right" role="form" method="post" action="user-dashboard.php">
												 <div class="input-group">
                                          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
											<input type="text" class="form-control" placeholder="Email" name="email" required />
                                          </div>
                                           <div class="input-group">
                                       <span class="input-group-addon"><i class="fa fa-lock" style="font-size: 20px;"></i></span>
											<input type="password" class="form-control" placeholder="Password" name="password" required />
      </div>
												<button type="submit" class="btn btn-default">Login</button>
											  </form>
				
		<?php endif; ?>
                         
                          
                        </div><!--/.navbar-collapse -->
                        
                      </div>
                    </nav>
                    
                    
     <!-- PC & Tablet -->              
                    
  <div id="header" class="hidden-xs hidden-sm">
  		 <div class="container">
              <div class="row">   
                     <div class="col-md-3 logo">
                            	<a href="index.php"><img alt="Axtivo" src="images/logo.png" /></a>
                       </div>   
                       
                       
                        <div class="col-md-6 menu">
                            	<ul>
                                	<li><a href="index.php"><i class="fa fa-home"></i></a></li>
                                    <li><a href="activities.php">ACTIVITIES</a></li>
                                    <li><a href="about-us.php">ABOUT US</a></li>
                                    <li><a href="faq.php">FAQ</a></li>
                                    <li><a href="contact.php">HOSTING</a></li>
                                </ul>
                       </div>   
                       
                       
                         <?php 
								 if (isset($_SESSION['axt_auth_user']) && $_SESSION['status']=="Active"):
								 ?>  
								
								  <!-- PC Login Button -->
                                
                                
									<div class="btn-group" style="padding-top: 15px;">
									  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-user"></i> <?php echo $_SESSION['firstname'];?> <span class="caret"></span>
									  </button>
									  <ul class="dropdown-menu" role="menu" id="partner-menu">
									  <li><a href="user-dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                      
                                      <?php if($_SESSION['user_type_id']==1):?>
										<li><a href="my-bookings.php"><i class="fa fa-calendar"></i> My Bookings</a></li>
										<li><a href="my-messages.php"><i class="fa fa-envelope"></i> My Messages</a></li>
                                        <li><a href="my-subscription.php"><i class="fa fa-credit-card"></i> My Subscription</a></li>
                  					<?php endif; ?>
                                    
                                     <?php if($_SESSION['user_type_id']==2):?>
                                     	<li><a href="my-bookings.php"><i class="fa fa-calendar"></i> My Bookings</a></li>
                                        <li><a href="my-activities.php"><i class="fa fa-file-text"></i> My Activities</a></li>
										<li><a href="my-messages.php"><i class="fa fa-envelope"></i> My Messages</a></li>
                                     <?php endif; ?>
                                     
										<li class="divider"></li>
										<li><a href="requests/logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
									  </ul>
									</div>
					
									<?php
									else:
								?>
                       
                        <div class="col-md-3 login-btn">
                            	<div class="btn-group" role="group" aria-label="...">
                           
                                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">LOGIN</button>
                                  <a href="register.php" class="btn btn-default">REGISTER</a>
                                </div>
                       </div>   
                       
                       <?php endif; ?>
                       
                       
                       
                       
               </div>            
       </div>
  
  </div>
  
  <!-- Login Modal -->
  
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog" style="width:660px;">
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">Login</h4>
							  </div>
							  <div class="modal-body">
								
										<form class="form-inline" method="post" action="user-dashboard.php">
											 <div class="input-group">
                                          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
											<input type="email" class="form-control" placeholder="Email" name="email" required />
                                          </div>
                                           <div class="input-group">
                                           <span class="input-group-addon"><i class="fa fa-lock"></i></span>
											<input type="password" class="form-control" placeholder="Password" name="password" required />
      </div>

									<button id="login"  type="submit" class="btn btn-primary" data-loading-text="Authenticating...">LOGIN</button>
									</form>
							  </div>

							</div>
						  </div>
					</div>