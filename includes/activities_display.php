<?php
session_start();

if($_POST){
	$by_location = $_POST['location'];
    $by_activity = $_POST['activity'];
    $by_date_from = $_POST['date_from'];
    $by_date_to = $_POST['date_to'];

  	$query = "SELECT 
	  	LISTING.id, 
	  	LISTING.image, 
	  	LISTING.listing_name, 
	  	LISTING.description, 
	  	LISTING_META.repeat_start, 
	  	LISTING_META.type 
	  	FROM LISTING 
	  	LEFT JOIN LISTING_META ON LISTING.id = LISTING_META.listing_id AND LISTING_META.id IN (
	  		SELECT MAX(id) 
	  		FROM LISTING_META 
	  		GROUP BY unique_id
	  	)
  	";
	$conditions = array();

	if($by_location != "") {
		$conditions[] = "LISTING.location_id='$by_location'";
	}

	if($by_activity != "") {
		$conditions[] = "LISTING.category_id='$by_activity'";
	}

	$sql = $query;

	if (count($conditions) > 0) {
		$sql .= " WHERE " . implode(' AND ', $conditions);
	}

	if($by_date_from !="" && $by_date_to !="" && count($conditions) == 0) {
			$sql = $sql ." WHERE (LISTING_META.repeat_start BETWEEN CAST('".date('Y-m-d',strtotime($by_date_from))."' AS DATE) AND CAST('".date('Y-m-d',strtotime($by_date_to))."' AS DATE) OR LISTING_META.repeat_end BETWEEN CAST('".date('Y-m-d',strtotime($by_date_from))."' AS DATE) AND CAST('".date('Y-m-d',strtotime($by_date_to))."' AS DATE) OR LISTING_META.repeat_start <= '".date('Y-m-d',strtotime($by_date_from))."' AND LISTING_META.repeat_end >= '".date('Y-m-d',strtotime($by_date_to))."')";
	}
	elseif ($by_date_from !="" && $by_date_to !="" && count($conditions) > 0) {
		$sql = $sql ." AND (LISTING_META.repeat_start BETWEEN CAST('".date('Y-m-d',strtotime($by_date_from))."' AS DATE) AND CAST('".date('Y-m-d',strtotime($by_date_to))."' AS DATE) OR LISTING_META.repeat_end BETWEEN CAST('".date('Y-m-d',strtotime($by_date_from))."' AS DATE) AND CAST('".date('Y-m-d',strtotime($by_date_to))."' AS DATE) OR LISTING_META.repeat_start <= '".date('Y-m-d',strtotime($by_date_from))."' AND LISTING_META.repeat_end >= '".date('Y-m-d',strtotime($by_date_to))."')";
	}
	//echo $sql;



	$queryUnion = "SELECT 
	  	LISTING.id, 
	  	LISTING.image, 
	  	LISTING.listing_name, 
	  	LISTING.description, 
	  	LISTING_META.repeat_start, 
	  	LISTING_META.type 
	  	FROM LISTING 
	  	LEFT JOIN LISTING_META ON LISTING.id = LISTING_META.listing_id AND LISTING_META.id IN (
	  		SELECT MAX(id) 
	  		FROM LISTING_META 
	  		GROUP BY unique_id
	  	)
  	";
	$conditionsUnion = array();

	if($by_location != "") {
		$conditionsUnion[] = "LISTING.location_id='$by_location'";
	}

	$InnerJoin = "";
	if($by_activity != "") {
		$InnerJoin = "INNER JOIN LISTING_CATEGORY ON LISTING.id = LISTING_CATEGORY.listing_id";
		$conditionsUnion[] = "LISTING_CATEGORY.category_id='$by_activity'";
	}

	$sqlUnion = $queryUnion.' '.$InnerJoin;

	if (count($conditionsUnion) > 0) {
		$sqlUnion .= " WHERE " . implode(' AND ', $conditionsUnion);
	}

	if($by_date_from !="" && $by_date_to !="" && count($conditionsUnion) == 0) {
			$sqlUnion = $sqlUnion ." WHERE (LISTING_META.repeat_start BETWEEN CAST('".date('Y-m-d',strtotime($by_date_from))."' AS DATE) AND CAST('".date('Y-m-d',strtotime($by_date_to))."' AS DATE) OR LISTING_META.repeat_end BETWEEN CAST('".date('Y-m-d',strtotime($by_date_from))."' AS DATE) AND CAST('".date('Y-m-d',strtotime($by_date_to))."' AS DATE) OR LISTING_META.repeat_start <= '".date('Y-m-d',strtotime($by_date_from))."' AND LISTING_META.repeat_end >= '".date('Y-m-d',strtotime($by_date_to))."')";
	}
	elseif ($by_date_from !="" && $by_date_to !="" && count($conditionsUnion) > 0) {
		$sqlUnion = $sqlUnion ." AND (LISTING_META.repeat_start BETWEEN CAST('".date('Y-m-d',strtotime($by_date_from))."' AS DATE) AND CAST('".date('Y-m-d',strtotime($by_date_to))."' AS DATE) OR LISTING_META.repeat_end BETWEEN CAST('".date('Y-m-d',strtotime($by_date_from))."' AS DATE) AND CAST('".date('Y-m-d',strtotime($by_date_to))."' AS DATE) OR LISTING_META.repeat_start <= '".date('Y-m-d',strtotime($by_date_from))."' AND LISTING_META.repeat_end >= '".date('Y-m-d',strtotime($by_date_to))."')";
	}

	$sqlAll = $sql.' UNION '.$sqlUnion;


	$listingSQL = mysqli_query($conn,$sqlAll);

}
else{
		$listingSQL = mysqli_query($conn, "SELECT DISTINCT LISTING.id, LISTING.image, LISTING.listing_name, LISTING.description, LISTING_META.repeat_start, LISTING_META.type FROM LISTING LEFT JOIN LISTING_META ON LISTING.id = LISTING_META.listing_id AND LISTING_META.id IN (SELECT MAX(id) FROM LISTING_META GROUP BY unique_id) where (LISTING_META.repeat_start<=CURDATE() AND LISTING_META.repeat_end >= CURDATE() OR LISTING_META.repeat_end IS NULL AND LISTING_META.repeat_interval IS NOT NULL AND LISTING_META.type!='Once') OR (LISTING_META.repeat_start>=CURDATE() AND LISTING_META.type='Once')");

}

echo '<div class="row row-flex row-flex-wrap" id="activity-item">';

if(mysqli_num_rows($listingSQL)>0){

while ($listingArray = mysqli_fetch_assoc($listingSQL)) {

	//$locationSQL = mysqli_query($conn, "SELECT * FROM LOCATION where id=".$listingArray['location_id']);
	//$locationArray=mysqli_fetch_assoc($locationSQL);

		echo '<div class="col-md-4" id="listing-panel" >
				<div class="panel panel-default">';

				$default_pic = "images/no-image.png";
				$check_pic = "graphics/".$listingArray['image'];
				if (empty($listingArray['image'])) {
					$display_pic = "<img src='".$default_pic."' class='img img-responsive' style='height:160px;width:100%;' />";
				} elseif (file_exists($check_pic)){
					$display_pic = "<img src='".$check_pic."' class='img img-responsive' style='height:160px;width:100%;' />";
				}
				else {
					$display_pic = "<img src='".$default_pic."' class='img img-responsive' style='height:160px;width:100%;' />";
				}
				echo $display_pic;

		echo'<div class="panel-body">
					<h2>'.$listingArray['listing_name'].'	</h2>
					<div class="info">';

		if ($listingArray['type'] == 'Once')
		{
				echo '<span class="activity-date"><i class="fa fa-calendar"></i> '.date('d-m-Y',strtotime($listingArray['repeat_start'])).'</span>';
		}

		$Descriptions = "";
		if(strlen($listingArray['description']) > 100){
			$Descriptions = trim(substr($listingArray['description'], 0, 50)).' ...';
		}else{
			$Descriptions = $listingArray['description'];
		}

		echo '</div>
					<p class="description" style="padding-top:10px; padding-bottom:10px;">'.$Descriptions.'</p>
					<form action="view-activity.php" method="post" >
					<input type="hidden" name="id" value="'.$listingArray['id'].'">
					<button class="btn btn-primary btn-lg" role="button" type="submit">
						<span>VIEW</span>
						<i class="fa fa-angle-right"></i>
						</button>
						</form>
				  </div>
				</div>
			  </div>
				<div class="clear"></div>
			 ';

 }
}
else{
	echo '<div class="alert alert-warning"> No results found.</div>';
}

 echo '</div>';
 ?>
