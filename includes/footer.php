<!-- Footer -->

<div id="footer">
  		 <div class="container">
         		<div class="row">
                		<div class="col-xs-12 col-md-3">
                        	<img src="images/logo-footer.png" />
                             <span class="credit">Axtivo &copy; 2017 | All Rights Reserved <br><a href="http://nemesys.mu">The Nemesys Difference </a></span>
                        </div>

                        <div class="col-md-3 hidden-xs hidden-sm">
                        	<ul class="footer-menu">
                            	<li><a href="index.php">Home </a></li>
                                <li><a href="activities.php">Activities </a></li>
                                <li><a href="about-us.php">About Us </a></li>
                            </ul>
                        </div>

                        <div class="col-md-3 hidden-xs hidden-sm">
                        <ul class="footer-menu">
                            	<li><a href="faq.php">FAQ </a></li>
                                <li><a href="press.php">Press </a></li>
                                <li><a href="contact.php">Hosting </a></li>
                            </ul>
                        </div>

                        <div class="col-xs-12 hidden-md hidden-lg">
                        	  <ul class="list-group" id="footer-menu">
                              	<li  class="list-group-item"><a href="index.php">Home </a></li>
                                <li  class="list-group-item"><a href="activities.php">Activities </a></li>
                                <li  class="list-group-item"><a href="about-us.php">About Us </a></li>
                                <li  class="list-group-item"><a href="faq.php">FAQ </a></li>
                                <li  class="list-group-item"><a href="press.php">Press </a></li>
                                <li  class="list-group-item"><a href="contact.php">Hosting </a></li>

                              </ul>
                        </div>





                        <div class="col-xs-12 col-md-3">
                        	<div class="follow">Follow Us</div>
                            <ul class="social-links">
                                <li><a href="https://twitter.com/axtivo_ireland" data-show-count="false"><img src="images/twitter.png"/></a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></li>
                                <li><img src="images/youtube.png" /></li>
                            </ul>

                        </div>
                  </div>

  </div>
</div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>



  </body>
</html>
