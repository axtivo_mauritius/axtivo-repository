<?php
error_reporting(0); 
header('Cache-Control: max-age=900');
?>
<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Axtivo is an activity platform in the Dublin region of Ireland, matching excellent activity and class Hosts, with our site Members.">
    <meta name="keywords" content="Activity, Class, Teacher, Coach, Fitness, Dance, Language, Creative, Arts, Martial Arts, Outdoors, Education, Music, Wellness, Nightlife, Kids, Teens, IT, Nightlife, Information Technology">
    <meta name="author" content="">
    <meta name="google-site-verification" content="PnFqZqEQ8sgMcsX_VQHpqIwWQImhv7lfwOxPck46tYY" /> 
    <link rel="icon" href="favicon.ico">

    <title>Axtivo, Dublin's only dedicated activity and class platform</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

    <!-- Custom styles for this template -->

    <link rel='stylesheet' type='text/css' href='css/fullcalendar.css' />
<link rel='stylesheet' type='text/css' href='css/fullcalendar.print.css' media='print' />
<link href="css/style.css" rel="stylesheet">
<link href="css/jquery.timepicker.css" rel="stylesheet">

    <!-- Just for debugging purposes -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

   <!-- <script src="js/jquery.min.js"></script>-->

    <link rel="stylesheet" type="text/css" href="css/validationEngine.jquery.css" />
    <link rel='stylesheet' type='text/css' href='css/jquery-ui-1.10.1.custom.css' />
    <script type='text/javascript' src='js/jquery-1.9.1.js'></script>
    <script type='text/javascript' src='js/jquery.ui.core.js'></script>
    <script type='text/javascript' src='js/fullcalendar.min.js'></script>
    <script type='text/javascript' src='js/jquery.ui.widget.js'></script>
    <script type='text/javascript' src='js/jquery.ui.position.js'></script>
    <script type='text/javascript' src='js/jquery.ui.menu.js'></script>
    <script type='text/javascript' src='js/jquery.ui.datepicker.js'></script>
    <script type='text/javascript' src='js/jquery.ui.autocomplete.js'></script>
    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <script type='text/javascript' src='js/validate.js'></script>
    <script type="text/javascript" src="js/captcha.js"></script>
    <script type='text/javascript' src='js/sisyphus.js'></script>
    <script src="js/jquery.price_format.js" type="text/javascript"></script>
    <script type='text/javascript' src='js/jquery.timepicker.min.js'></script>
    <script type='text/javascript' src='js/jquery.datepair.min.js'></script>

    <script type='text/javascript' src="js/css3-mediaqueries.js"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



  </head>

  <body>
