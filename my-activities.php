<?php
session_start();

if(!isset($_SESSION['axt_auth_user'])){
	die('You are not authorised to view this page.');
}

if($_SESSION['user_type_id']!=2){
	die('You are not authorised to view this page.');
}

include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');


?>

<script>
$(document).ready(function(){

$('#calendar').fullCalendar({

			editable: false,

			events: "json-events.php",
		});

});
</script>

<div class="inside-page">
    <div class="container">

         <h1>MY ACTIVITIES</h1>

        <div id='calendar' style="margin-bottom:30px;"></div>

         <?php

		 $listingSQL = mysqli_query($conn, "SELECT * FROM LISTING where user_id=".$_SESSION['user_id']." order by id desc");

		 if (mysqli_num_rows($listingSQL)!=0) {
			 echo '<div id="no-more-tables table-responsive">
           					 <table class="col-md-12 table-bordered table-striped table-condensed cf">
									<thead class="cf">
									  <tr>
										<th>#</th>
										<th>Activity Name </th>
										<th>Vacancies</th>
										<th>Action</th>

									  </tr>
									</thead>
									<tbody>';
							 $i = 0;

							while ($listingArray = mysqli_fetch_assoc($listingSQL))  {

								echo '  <tr>
											<td data-title="#">'.$listingArray['id'].'</td>
											<td data-title="Activity Name">'.$listingArray['listing_name'].'</td>
											<td data-title="Vacancies">'.$listingArray['vacancies'].'</td>
											<td data-title="Action"> &nbsp;


											<form action="view-activity.php" method="post" style="margin-right:5px; float:left;">
												<input type="hidden" name="id" value="'.$listingArray['id'].'">
												<input type="hidden" name="btn-action" value="host-view">
												<button class="btn btn-default btn-action" type="submit"><i class="fa fa-eye"></i> View</button>
											</form>

											<form action="edit-activity.php?edit" method="post" style="margin-right:5px; float:left">
												<input type="hidden" name="id" value="'.$listingArray['id'].'">
												<input type="hidden" name="btn-action" value="host-view">
												<button class="btn btn-primary btn-action" type="submit"><i class="fa fa-edit"></i> Edit</button>
											</form>

											</td>
										</tr>';
										 $i++;
									}
							echo '</tbody>
								  </table>
								  </div>
								 ';
		 }

		 else {
						echo '<div class="alert alert-warning">

            <h4>No Activities Found</h4>
            <p>You can create an activity by pressing on the button below. </p><br />

            <p>
            <a href="create-activity.php" class="btn btn-primary">Create New</a>
            </p>
          </div>';
						}



		 if($_SESSION['user_type_id']==2){
			echo '<div class="row col-xs-12"><br><a class="btn btn-primary" href="create-activity.php"><i class="fa fa-plus"></i> Add New</a></div>' ;
		 }
		 ?>






     </div>
 </div>


 <?php include_once('includes/footer.php'); ?>
