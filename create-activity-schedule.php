<?php session_start();
error_reporting(0);

if(!isset($_SESSION['axt_auth_user']) || $_SESSION['user_type_id']!=2){
	die('Access Denied.');
}

include_once('includes/mydb.php');

include_once 'lib/dm/DataManager.php';

$id = isset($_POST['id'])?$_POST['id']:0;

if(isset($_GET['action']))
{


        //inserting/updating row
      if($_GET['action']=="save"){

			if(!$_POST){
				die('Action denied.');
			}

      $data = $_POST;

			$data['user_id']=$_SESSION['user_id'];

			$uniqid = uniqid();


			$listing_name = $data['listing_name'];
			$description = $data['description'];
			$image = $uniqid.$_FILES["image"]["name"];
			$category_id = $data['category_id'];
			$vacancies = $data['vacancies'];
			$location_id = $data['location_id'];
			$venue = $data['venue'];
			$user_id = $data['user_id'];

			$insertListingQuery = "INSERT INTO LISTING (listing_name, description, image, category_id, vacancies,location_id, venue, user_id) VALUE ('$listing_name', '$description', '$image', $category_id, $vacancies, $location_id, '$venue', $user_id)";
			$insertListingMeta = mysqli_query($conn, $insertListingQuery);
			$id = mysqli_insert_id($conn);


			$type = $data['selectRepeat'];
			$repeat = $data['selectRepeatEvery'];
			$repeatEvery = (int)$repeat;
			$repeatOn = $data['inputRepeatOn'];
			$start = date_format(date_create($data['inputStartsOn']),"Y-m-d");
			$end = empty($data['inputEndsOn']) ? "" : date_format(date_create($data['inputEndsOn']),"Y-m-d");;
			$startTime = $data['inputTimeStart'];
			$endTime = $data['inputTimeEnd'];
			$currDate = new DateTime($start);

			$insertQuery = '';

			if ($type == 'once')
			{
				$insertQuery = "INSERT INTO LISTING_META (listing_id, repeat_start, start_time, end_time, type, unique_id) VALUES (".$id.", '".$start."', '".$startTime."', '".$endTime."', 'Once', '".$uniqid."')";
				$insertListingMeta = mysqli_query($conn, $insertQuery);
			}
			else if ($type == 'daily')
			{
				$insertQuery = "INSERT INTO LISTING_META (listing_id, repeat_start, repeat_interval, repeat_end, start_time, end_time, type, unique_id) VALUES (".$id.", '".$start."', '".$repeat."', '".$end."', '".$startTime."', '".$endTime."', 'Daily', '".$uniqid."')";
				$insertListingMeta = mysqli_query($conn, $insertQuery);
			}
			else if ($type == 'weekly')
			{

				for ($x = 0; $x <7; $x++)
				{
					$currDate->modify('+1 day');

					$aDate = date('w', strtotime($currDate->format('Y-m-d')));

					foreach($repeatOn as $day){
						if ($aDate == $day)
						{
									$endDate = empty($end) ? "NULL" : "'".$end."'";
									$insertQuery = "INSERT INTO LISTING_META (listing_id, repeat_start, repeat_interval, repeat_end, start_time, end_time, type, unique_id, weekly_days) VALUES (".$id.", '".$currDate->format("Y-m-d")."', '".(string)($repeatEvery*7)."',".$endDate.", '".$startTime."', '".$endTime."', 'Weekly','".$uniqid."','".implode(",",$repeatOn)."')";
									$insertListingMeta = mysqli_query($conn, $insertQuery);
						}
					}

				}

			}
			else if ($type == 'monthly')
			{

			}
				 uploadPicture($image);
         header('location: my-activities.php?success');
				 exit;
        }

}

function uploadPicture($image){

	$target_dir = "graphics/";

	if (!file_exists($target_dir)) {
	    mkdir($target_dir, 0777, true);
	}


	$target_file = $target_dir . basename($image);

	$writeable = is_writable($target_file);
	echo $writeable;

	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
	    $check = getimagesize($_FILES["image"]["tmp_name"]);
	    if($check !== false) {
	        //echo "File is an image - " . $check["mime"] . ".";
	        $uploadOk = 1;
	    } else {
	        //echo "File is not an image.";
	        $uploadOk = 0;
	    }
	}
	// Check if file already exists
	if (file_exists($target_file)) {
	    //echo "Sorry, file already exists.";
	    $uploadOk = 0;
	}
	// Check file size
	if ($_FILES["image"]["size"] > 5000000) {
	    //echo "Sorry, your file is too large.";
	    $uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
	    //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    //echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
	    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
	        //echo "The file ". basename( $image). " has been uploaded.";
	    } else {
	        //echo "Sorry, there was an error uploading your file.";
	    }
	}

	header('location: my-activities.php?success');
	exit;
}

include_once('includes/header.php');
include_once('includes/menu.php');
 ?>
