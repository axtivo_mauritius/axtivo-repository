<?php
//Start session
session_start();

if(!isset($_SESSION['axt_auth_user'])){
	die('You are not authorised to view this page.');
}

if($_SESSION['user_type_id']!=2){
	die('You are not authorised to view this page.');
}

//Connect to DB
include_once('includes/mydb.php');

//Query for calendar


	    $now = strtotime("yesterday");

	    $pushToFirst = -11;
	    for($i = $pushToFirst; $i < $pushToFirst+60; $i++)
	    {
	        $now = strtotime("+".$i." day");
	        $year = date("Y", $now);
	        $month = date("m", $now);
	        $day = date("d", $now);
	        $nowString = $year . "-" . $month . "-" . $day;
	        $week = (int) ((date('d', $now) - 1) / 7) + 1;
	        $weekday = date("N", $now);

	        $sql = "SELECT EV.*
	                FROM `LISTING` EV
	                RIGHT JOIN `LISTING_META` EM1 ON EM1.`listing_id` = EV.`id`
	                WHERE user_id = ".$_SESSION['user_id']." AND ((repeat_start = '$nowString' AND repeat_end IS NULL) OR
									( DATEDIFF( '$nowString', repeat_start ) % repeat_interval = 0 AND repeat_start <= DATE('$nowString') AND (repeat_end >= DATE('$nowString') OR repeat_end IS NULL))
	                OR (
	                    (repeat_year = $year OR repeat_year = '*' )
	                    AND
	                    (repeat_month = $month OR repeat_month = '*' )
	                    AND
	                    (repeat_day = $day OR repeat_day = '*' )
	                    AND
	                    (repeat_week = $week OR repeat_week = '*' )
	                    AND
	                    (repeat_weekday = $weekday OR repeat_weekday = '*' )
	                    AND repeat_start <= DATE('$nowString')
	                ))";

									//error_log($sql);

	        foreach ($conn->query($sql) as $row) {


							$eventArray['title'] = $row['listing_name'];
							$eventArray['start'] =  $nowString ;
							$eventArray['url'] = "/view-activity.php?id=".$row['id'];
							$events[] = $eventArray;

	        }

	    }



//Output to calendar using JSON
header('Content-type: application/json');

echo json_encode($events);

?>
