<?php
session_start();


if(!isset($_SESSION['axt_auth_user']) || $_SESSION['user_type_id']!=2){
	die('Access Denied.');
}


if($_POST['id']>0){
	$listingID = $_SESSION['listing_id'] = $_POST['id'];
}
else{
	$listingID = $_SESSION['listing_id'];

}

include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');

$listingSQL = mysqli_query($conn, "SELECT * FROM LISTING where id=".$listingID);
$listingArray = mysqli_fetch_assoc($listingSQL);

$listingMetaSQL = mysqli_query($conn, "SELECT * FROM LISTING_META WHERE id IN (SELECT MAX(id) FROM LISTING_META GROUP BY unique_id) AND listing_id = ".$listingID);


?>

<script>
$(document).ready(function(){

	$( "#inputStartsOn" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
        $("#inputStartsOn").valid();
    } });


		$( "#inputEndsOn" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
        $("#inputEndsOn").valid();
    } });

		$('.time').timepicker({
		    'showDuration': true,
		    'timeFormat': 'H:i'
		});

		var time = document.getElementById('inputTime');
		var timeDatepair = new Datepair(time);

		$('#repeatOnEveryBox').hide();
		$('#repeatEveryBox').hide();
		$('#EndsOnBox').hide();
		$('#startOnLabel').text('Date: ');

	$('#selectRepeat').change(function(){

		var repeatType = $(this).val();

		if (repeatType == "daily")
		{
				$('#repeatOnEveryBox').hide();
				$('#repeatEveryBox').show();
				$('#repeatText').text("days");
				$('#startOnLabel').text('Starts On: ');
				$('#EndsOnBox').show();
		}
		else if ( repeatType == "weekly"){
				$('#repeatOnEveryBox').show();
				$('#repeatEveryBox').show();
				$('#repeatText').text("weeks");
				$('#startOnLabel').text('Starts On: ');
				$('#EndsOnBox').show();
		}
		else if ( repeatType == "monthly"){
				$('#repeatOnEveryBox').hide();
				$('#repeatText').text("months");
				$('#EndsOnBox').show();
				$('#startOnLabel').text('Date: ');
		}
		else if (repeatType == "once"){
				$('#repeatOnEveryBox').hide();
				$('#repeatEveryBox').hide();
				$('#EndsOnBox').hide();
				$('#startOnLabel').text('Date: ');
		}

	});


});
</script>

<div class="inside-page">
    <div class="container">

         <h1><?php echo $listingArray['listing_name']; ?></h1>

         <div class="row">

         		<div class="col-md-8">
        					<div id="listing-div">

                                    <div class="row">

                                        <div class="col-md-12">

																					<div id="no-more-tables">
																						<?php 		 if (mysqli_num_rows($listingMetaSQL)!=0) { ?>
																	            <table class="col-md-12 table-bordered table-striped table-condensed cf">
																	 									<thead class="cf">
																	 									  <tr>
																	 										<th>Event Type</th>
																	 										<th>Date</th>
																											<th>Time</th>
																	 										<th>Day</th>
																	 										<th>Action</th>

																	 									  </tr>
																	 									</thead>
																	 									<tbody>
																											<?php while ($listingMetaArray = mysqli_fetch_assoc($listingMetaSQL))  { ?>
																											<tr>
																														<td data-title="Event Type"><?php echo $listingMetaArray[type]; ?> Event</td>
																														<td data-title="Date"><?php echo date('d-m-Y',strtotime($listingMetaArray[repeat_start])); echo ($listingMetaArray[repeat_end] === NULL ?  " " : " - ".date('d-m-Y',strtotime($listingMetaArray[repeat_end])));?> </td>
																														<td data-title="Time"><?php echo date('H:i',strtotime($listingMetaArray[start_time]))." - ". date('H:i',strtotime($listingMetaArray[end_time])) ; ?> </td>
																														<td data-title="Day">
																														<?php

																														$weekDaysArray = explode(",", $listingMetaArray[weekly_days]);
																														$i = 0;
																														$len = count($weekDaysArray);
																														if (strlen($listingMetaArray[weekly_days])  == 0 && $listingMetaArray[type] == "Once")
																														{
																															$onceDay = date('w', $listingMetaArray[repeat_start]);
																															$weekDaysArray[0] = $onceDay;
																														}

																														foreach ($weekDaysArray as $weekDay) {
																															switch ($weekDay) {
																																case '0':
																																	echo "Sunday";
																																	break;
																																case '1':
																																	echo "Monday";
																																	break;
																																case '2':
																																	echo "Tuesday";
																																	break;
																																case '3':
																																	echo "Wednesday";
																																	break;
																																case '4':
																																	echo "Thursday";
																																	break;
																																case '5':
																																	echo "Friday";
																																	break;
																																case '6':
																																	echo "Saturday";
																																	break;
																																default:
																																	# code...
																																	break;
																															}

																															if ($i != $len-1 )
																															{
																																echo ", ";
																															}

																															$i++;

																														}

																														?>
																														</td>
																														<td data-title="Action">
																															<form action="delete-activity-schedule.php?action=delete" method="post" style="margin-right:5px">
																															<?php echo '<input type="hidden" name="id" value="'.$listingMetaArray["unique_id"].'"'?>
																															<?php echo '<input type="hidden" name="listing_id" value="'.$listingArray["id"].'"'?>
																															<input type="hidden" name="btn-action" value="host-view">
																															<button class="btn btn-primary btn-action" type="submit"><i class="fa fa-trash"></i> Delete</button>
																														</form>
																														</td>
																											</tr>
																											<?php } ?>

																										</tbody>
																									</table>
																								<?php }
																								else {
																									echo '<div class="alert alert-warning">

																									<h4>No Schedule Found</h4>
																									<p>You can create a schedule by pressing on the button Add below. </p><br />

																								</div>';

																								}?>
																								</div>

                                        </div>




                                    </div>



                            <div class="row" align="left" style="margin-top:20px;">

															<a class="btn btn-primary btn-lg" data-toggle="modal" data-target="#AddScheduleModal">
																						 <i class="fa fa-plus"></i>
																							<span>Add</span>

																							</a>

                            		<a class="btn btn-primary btn-lg btn-back" href="my-activities.php">
                                               <i class="fa fa-angle-left"></i>
                                                <span>BACK</span>

                                                </a>
                            </div>



                            </div>
                 </div>
          </div>

     </div>
 </div>

 <!-- Add Schedule Modal -->

 <div class="modal fade" id="AddScheduleModal" tabindex="-1" role="dialog" aria-labelledby="AddScheduleModalLabel" aria-hidden="true">
						 <div class="modal-dialog" style="width:660px;">
						 <div class="modal-content">
							 <div class="modal-header">
							 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							 <h4 class="modal-title" id="AddScheduleModalLabel">Add New</h4>
							 </div>
							 <div class="modal-body">

									 <form class="form-horizontal" method="post" action="create-activity-schedule.php?action=save" validate>

										 <?php echo '<input type="hidden" name="id" value="'.$listingID.'">'; ?>
										 <div class="form-group" >
										    <label for="selectRepeat" class="col-sm-4 control-label">Repeats:</label>
										    <div class="col-sm-6">
										      <select class="form-control" name="selectRepeat" id="selectRepeat" required>
														<option value="once">One Time</option>
														<option value="daily">Daily</option>
														<option value="weekly">Weekly</option>
													</select>
										    </div>
										  </div>


											<div class="form-group" id="repeatEveryBox">
												 <label for="selectRepeatEvery" class="col-sm-4 control-label">Repeat every:</label>
												 <div class="col-sm-4">
													 <select class="form-control" id="selectRepeatEvery" name="selectRepeatEvery" required>
														 <option value="1">1</option>
														 <option value="2">2</option>
														 <option value="3">3</option>
														 <option value="4">4</option>
														 <option value="5">5</option>
														 <option value="6">6</option>
														 <option value="7">7</option>
														 <option value="8">8</option>
														 <option value="9">9</option>
														 <option value="10">10</option>
														 <option value="11">11</option>
														 <option value="12">12</option>
														 <option value="13">13</option>
														 <option value="14">14</option>
														 <option value="15">15</option>
														 <option value="16">16</option>
														 <option value="17">17</option>
														 <option value="18">18</option>
														 <option value="19">19</option>
														 <option value="20">20</option>
													 </select>
												 </div>
												 <div class="col-sm-2" id="repeatText" style="padding-top: 7px; text-align: left;"> days</div>
											 </div>

											 <div class="form-group" id="repeatOnEveryBox">
 												 <label for="selectRepeatOn" class="col-sm-4 control-label">Repeat on:</label>
 												 <div class="col-sm-6">
													 <div class="checkbox">
														 <label>
															 <input name="inputRepeatOn[]" value="0" type="checkbox"> S 	&nbsp;
														 </label>
														 <label>
															 <input name="inputRepeatOn[]" value="1" type="checkbox"> M 	&nbsp;
														 </label>
														 <label>
															 <input name="inputRepeatOn[]" value="2" type="checkbox"> T 	&nbsp;
														 </label>
														 <label>
															 <input name="inputRepeatOn[]" value="3" type="checkbox"> W 	&nbsp;
														 </label>
														 <label>
															 <input name="inputRepeatOn[]" value="4" type="checkbox"> T 	&nbsp;
														 </label>
														 <label>
															 <input name="inputRepeatOn[]" value="5" type="checkbox"> F 	&nbsp;
														 </label>
														 <label>
															 <input name="inputRepeatOn[]" value="6" type="checkbox"> S 	&nbsp;
														 </label>

														</div>
 												 </div>
 											 </div>

											 <div class="form-group" id="StartsOnBox">
											    <label for="inputStartsOn" id="startOnLabel" class="col-sm-4 control-label" required>Starts On:</label>
											    <div class="col-sm-6">
											      <input class="form-control" id="inputStartsOn" name="inputStartsOn" type="text"/>
											    </div>
											  </div>

												<div class="form-group" id="EndsOnBox">
													 <label for="inputEndsOn" class="col-sm-4 control-label">Ends On:</label>
													 <div class="col-sm-6">
														 <input class="form-control" id="inputEndsOn" name="inputEndsOn" type="text"/>
													 </div>
												 </div>

												 <div id="inputTime">
													 <div class="form-group">
														 <label for="inputTimeStart" class="col-sm-4 control-label" required>Start Time:</label>
															 <div class="col-sm-3">
															 		<input class="form-control time start ui-timepicker-input" id="inputTimeStart" name="inputTimeStart" type="text"/>
														 		</div>
													 </div>
													 <div class="form-group">
														 <label for="inputTimeEnd" class="col-sm-4 control-label" required>End Time:</label>
															 <div class="col-sm-3">
																 <input class="form-control time end ui-timepicker-input" id="inputTimeEnd" name="inputTimeEnd" type="text"/>
															 </div>
													 </div>
												</div>



												 <div class="form-group">
													 <div class="col-sm-offset-2 col-sm-10">
														 <button type="submit" class="btn btn-default">Save</button>
													 </div>
												 </div>



								 </form>
							 </div>

						 </div>
						 </div>
				 </div>

 <?php include_once('includes/footer.php'); ?>
