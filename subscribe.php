<?php 
session_start();

if(!isset($_SESSION['axt_auth_user'])){
	die('You are not authorised to view this page.');
}
 
include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');

if($_POST['action']=='renew'){
	
	$subscriptionSQL = mysqli_query($conn, "select * from SUBSCRIPTION where id=".$_POST['id']);
	$subscriptionArray = mysqli_fetch_assoc($subscriptionSQL);
	
	//Penetration Check
	if($subscriptionArray['user_id']!=$_SESSION['user_id']){
			die('You are not authorised to view this page.');
	}
	
}

$userSQL = mysqli_query($conn, "SELECT * FROM USER where id=".$_SESSION['user_id']);
$userArray = mysqli_fetch_assoc($userSQL);
 
?>

<script>
$(document).ready(function(){	
 
});		  
</script>

<div class="inside-page">
    <div class="container">
        
         <h1>SUBSCRIBE </h1>  
 
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Billed To:</strong><br>
    					<?php echo $userArray['firstname']." ".$userArray['surname']; ?><br>
    					<?php echo $userArray['address_1']; ?><br>
    					<?php echo $userArray['address_2']; ?><br>
    				 
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    					<strong>Order Date:</strong><br>
    					<?php echo date('d-m-Y'); ?><br><br>
    				</address>
    			</div>
    		</div>
   
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Order summary</strong></h3>
    			</div>
    			
                <div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Plan</strong></td>
        							<td class="text-center"><strong>Price</strong></td>
        							<td class="text-center"><strong>Credits</strong></td>
        							<td class="text-right"><strong>Total</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							 
    							<tr>
    								<td>Monthly Plan</td>
    								<td class="text-center">&euro; 29.99 </td>
    								<td class="text-center">31</td>
    								<td class="text-right">&euro; 29.99</td>
    							</tr>
                              
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right" style="border-bottom: 4px double rgb(204, 204, 204);"><b>&euro; 29.99</b></td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		
            </div>
     
         </div>
         
        </div> 
        
        
        
        <div class="col-xs-12" align="right">
                <form action="charge.php" method="POST">
                      <script
                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                        data-key="pk_test_4MCgVC6wq3itl4mwb2dvK8we"
                        data-amount="2999"
                        data-name="AXTIVO"
                        data-description="Online Payment"
                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                        data-locale="auto"
                        data-zip-code="true"
                        data-currency="eur">
                      </script>
                    </form>
           </div> 
            
        
     </div>  
 </div>
 
 
 <?php include_once('includes/footer.php'); ?>