<?php 
session_start();

include_once('includes/header.php');
include_once('includes/menu.php');
?>

<script>
$(document).ready(function(){	
 
});		  
</script>

<div class="inside-page">
    <div class="container">
        
         <h1>PRIVACY POLICY</h1>  
        
<p><strong>SECTION 1 - WHAT DO WE DO WITH YOUR INFORMATION?</strong><br />
  <br />
When you register with us, we collect the personal information you give us such  as your name, address and email address.<br />
<br />
When you browse our website, we also automatically receive your computer&rsquo;s  internet protocol (IP) address in order to provide us with information that  helps us learn about your browser and operating system.<br />
<br />
Email marketing (if applicable): with your permission, we may send you emails  about our community, new products and other updates.<br />
<br />
<br />
<strong>SECTION 2 - CONSENT</strong><br />
<br />
How do you get my consent?<br />
<br />
When you provide us with personal information to complete a transaction, verify  your credit card, host an activity or book an activity, we imply that you  consent to our collecting it and using it for that specific reason only.<br />
<br />
If we ask for your personal information for a secondary reason, like marketing,  we will either ask you directly for your express consent, or provide you with  an opportunity to say no.<br />
<br />
 How do I withdraw my consent?<br />
<br />
If after you opt-in, you change your mind, you may withdraw your consent for us  to contact you, for the continued collection, use or disclosure of your  information, at anytime, by contacting us at info@axtivo.com or mailing us at:  Zeetan Limited, 17 Laragh Close, The Donahies, Donaghmede, Dublin 13, L, D13  X963, Ireland.<br />
<br />
<br />
<strong>SECTION 3 - DISCLOSURE</strong><br />
<br />
We may disclose your personal information if we are required by law to do so or  if you violate our Terms of Service.<br />
<br />
<br />
<strong>SECTION 4 – HOSTING</strong><br />
<br />
Our website is hosted on internet servers in France. Your data is stored  through our website's data storage, database and our general application. We  store your data on a secure server behind a firewall.<br />
<br />
<br />
<strong>SECTION 5 - THIRD-PARTY SERVICES</strong><br />
<br />
In general, the third-party providers used by us will only collect, use and  disclose your information to the extent necessary to allow them to perform the  services they provide to us.<br />
<br />
However, certain third-party service providers, such as payment gateways and  other payment transaction processors, have their own privacy policies in  respect to the information we are required to provide to them for your  purchase-related transactions.<br />
<br />
For these providers, we recommend that you read their privacy policies so you  can understand the manner in which your personal information will be handled by  these providers.<br />
<br />
In particular, remember that certain providers may be located in or have  facilities that are located in a different jurisdiction than either you or us.  So if you elect to proceed with a transaction that involves the services of a  third-party service provider, then your information may become subject to the  laws of the jurisdiction(s) in which that service provider or its facilities  are located.<br />
<br />
Once you leave our website or are redirected to a third-party website or  application, you are no longer governed by this Privacy Policy or our website&rsquo;s  Terms of Service.<br />
<br />
 Links<br />
<br />
When you click on links on our store, they may direct you away from our site.  We are not responsible for the privacy practices of other sites and encourage  you to read their privacy statements.<br />
<br />
<br />
<strong>SECTION 6 - SECURITY</strong><br />
<br />
To protect your personal information, we take reasonable precautions and follow  industry best practices to make sure it is not inappropriately lost, misused,  accessed, disclosed, altered or destroyed.<br />
<br />
<br />
<strong>SECTION 7 - AGE OF CONSENT</strong><br />
<br />
By using this site, you represent that you are at least the age of majority in  Ireland.<br />
<br />
<br />
<strong>SECTION 8 - CHANGES TO THIS PRIVACY POLICY</strong><br />
<br />
We reserve the right to modify this privacy policy at any time, so please  review it frequently. Changes and clarifications will take effect immediately  upon their posting on the website. If we make material changes to this policy,  we will notify you here that it has been updated, so that you are aware of what  information we collect, how we use it, and under what circumstances, if any, we  use and/or disclose it.<br />
<br />
If our community is acquired or merged with another company, your information  may be transferred to the new owners so that we may continue to sell products  to you.<br />
<br />
<br />
<strong>QUESTIONS AND CONTACT INFORMATION</strong><br />
<br />
If you would like to: access, correct, amend or delete any personal information  we have about you, register a complaint, or simply want more information  contact our Privacy Compliance Officer at info@axtivo.com or by mail at Zeetan  Limited<br />
<br />
Re: Privacy Compliance Officer<br />
<br />
17 Laragh Close, The Donahies, Donaghmede, Dublin 13, L, D13 X963, Ireland<br />
</p>

           
        
     </div>  
 </div>
 
 
 <?php include_once('includes/footer.php'); ?>