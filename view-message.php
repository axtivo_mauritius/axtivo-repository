<?php 
session_start();

if(!isset($_SESSION['axt_auth_user'])){
	die('You are not authorised to view this page.');
}
 
include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');

$messagesSQL = mysqli_query($conn, "select * from MESSAGES where id=".$_POST['id']);
$messagesArray = mysqli_fetch_assoc($messagesSQL);

$bookingSQL = mysqli_query($conn, "SELECT * FROM BOOKING where id=".$messagesArray['booking_id']);
$bookingArray = mysqli_fetch_assoc($bookingSQL);	
 
$listingSQL = mysqli_query($conn, "SELECT * FROM LISTING where id=".$bookingArray['listing_id']);
$listingArray = mysqli_fetch_assoc($listingSQL);

$customerSQL = mysqli_query($conn, "SELECT * FROM USER where id=".$_SESSION['user_id']);
$customerArray = mysqli_fetch_assoc($customerSQL);

//Penetration Check
if($_SESSION['user_type_id']==1){
	if($messagesArray['member_id']!=$_SESSION['user_id']){
		die('You are not authorised to view this page.');
	}
}
elseif($_SESSION['user_type_id']==2){
	if($messagesArray['host_id']!=$_SESSION['user_id']){
		die('You are not authorised to view this page.');
	}
}
else{
	die('You are not authorised to view this page.');
}

?>

<script>
$(document).ready(function(){	
 
});		  
</script>

<div class="inside-page">
    <div class="container">
        
         <h1>VIEW MESSAGE </h1>  
         
         <div style="padding:0; margin:0 auto;" align="center" class="panel panel-default">
	<table cellspacing="0" cellpadding="0" border="0" id="main" width="690">
		<tbody>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" border="0" id="page" background="#fff" width="660">
							<tr>
								<td id="logo">
									 <h1><img src="http://axtivo.com/schedule_tmpl/logo.jpg" alt="logo"  border="0"></h1>
								</td>
							</tr>
							
							<tr>
								<td id="banner">
									<img src="http://axtivo.com/schedule_tmpl/email-travel-banner.jpg" alt="banner" border="0"></a>
								</td>
							</tr>
							
							<tr>
								<td>
									<h2><?php echo $messagesArray['subject']; ?></h2>
				
												<p style="text-align:left;">
												<?php 
												 
												 echo '<table width="100%" border="0" cellpadding="0" id="order" class="table">
													  <tr style="background:#f2f2f2;">
														<td width="30%"><strong>Customer Name</strong></td>
														<td>'.$customerArray['firstname'].' '.$customerArray['surname'].'</td>
													  </tr>
													  <tr>
														<td><strong>Activity Booked </strong></td>
														<td>'.$listingArray['listing_name'].'</td>
													  </tr>
													 <tr style="background:#f2f2f2;">
														<td><strong>Date </strong></td>
														<td>'.date('d-m-Y',strtotime($listingArray['start_date'])).'</td>
													  </tr>
													  <tr>
														<td><strong>Time</strong></td>
														<td>'
															.$listingArray['time'].
														'</td>
													  </tr>
													  
													  <tr style="background:#f2f2f2;">
														<td><strong>Venue</strong></td>
														<td>'.$listingArray['venue'].'</td>
													  </tr>
													 
													</table>';
												
												
												
												?>							  </p>
									
										</td>
								</tr>	
										
								<tr>
									<td>
											<img src="http://axtivo.com/schedule_tmpl/separator.gif" alt="separator">
											
											<h2>Contact Us</h2>
												
											<table width="660" border="0" cellpadding="10" id="contact"  background="#fff" class="table">
												  <tr>
													<td valign="middle" ><img src="http://axtivo.com/schedule_tmpl/email.gif" alt="map" >   &nbsp;&nbsp;info@axtivo.com </td>
 
												
													<td valign="middle"  ><img src="http://axtivo.com/schedule_tmpl/web.gif" alt="phone"><a href="http://www.axtivo.com" target="_blank"> &nbsp;&nbsp;www.axtivo.com</a></td>
												  </tr>
												</table>

								</td>
								
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>

								<p>Axtivo &copy; 2017. All rights reserved. </p>
					</td>
				
				</tr>
		</tbody>
	</table>

</div>
         
          <div class="row" align="left" style="margin-top:20px;">
                            		<a class="btn btn-primary btn-lg btn-back" href="my-messages.php">
                                               <i class="fa fa-angle-left"></i>
                                                <span>BACK</span>
                                                
                                                </a>
                            </div>	
        
     </div>  
 </div>
 
 
 <?php include_once('includes/footer.php'); ?>