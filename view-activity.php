<?php
session_start();

if($_POST['id']>0){
	$listingID = $_SESSION['listing_id'] = $_POST['id'];
}
else if ($_GET['id']>0) {
	$listingID = $_SESSION['listing_id'] = $_GET['id'];
}
else{
	$listingID = $_SESSION['listing_id'];

}

include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');

$listingSQL = mysqli_query($conn, "SELECT * FROM LISTING where id=".$listingID);
$listingArray = mysqli_fetch_assoc($listingSQL);

$userSQL = mysqli_query($conn, "SELECT * FROM USER where id=".$listingArray['user_id']);
$userArray = mysqli_fetch_assoc($userSQL);

$categorySQL = mysqli_query($conn, "SELECT * FROM CATEGORY where id=".$listingArray['category_id']);
$categoryArray = mysqli_fetch_assoc($categorySQL);

$locationSQL = mysqli_query($conn, "SELECT * FROM LOCATION where id=".$listingArray['location_id']);
$locationArray = mysqli_fetch_assoc($locationSQL);

$listingMetaSQL = mysqli_query($conn, "SELECT * FROM LISTING_META WHERE id IN (SELECT MAX(id) FROM LISTING_META GROUP BY unique_id) AND listing_id = ".$listingID);
$listingMetaArray = mysqli_fetch_assoc($listingMetaSQL);

?>

<script>
$(document).ready(function(){

});
</script>

<div class="inside-page">
    <div class="container">

         <h1><?php echo $listingArray['listing_name']; ?></h1>

         <div class="row">

                <div class="col-md-4" id="profile-panel">
                	 <div class="panel panel-default">
                      <div class="panel-body">
												<?php
												$default_pic = "images/no-image.png";
												$check_pic = "graphics/".$listingArray['image'];
												if (empty($listingArray['image'])) {
											    $display_pic = "<img src='".$default_pic."' class='img img-responsive' />";
											  } elseif (file_exists($check_pic)){
											    $display_pic = "<img src='".$check_pic."' class='img img-responsive'  />";
											  }
												else {
													$display_pic = "<img src='".$default_pic."' class='img img-responsive'  />";
												}
                       		echo $display_pic;
												?>

                        <div class="panel-text" style="padding-bottom:0;">
                            <h3> <?php echo $userArray['business_name']; ?></h3>
														<h5> <?php // echo in e-ticket instead echo empty($userArray['telephone']) ? "" : "<i class='fa fa-phone' aria-hidden='true'></i> ".$userArray['telephone']; ?></h5> 

 						</div>
                      </div>
                    </div>
                </div>


         		<div class="col-md-8">
        					<div id="listing-div">

                            		<div class="well"><?php echo $listingArray['description']; ?></div>


                                    <div class="row">

                                        <div class="col-md-6">
                                        	 <dl class="horizontal-dl">
                                             	<dt><label>Category</label></dt>
                                                <dd><?php echo $categoryArray['category_name']; ?></dd>
                                             </dl>

																						 <dl class="horizontal-dl">
																							<dt><label>Location</label></dt>
																							 <dd><?php echo $locationArray['location_name']; ?></dd>
																						</dl>


                                           <dl class="horizontal-dl">
                                             	<dt><label>Venue</label></dt>
                                              <dd><?php echo $listingArray['venue'];  ?></dd>
                                           </dl>
                                       		<dl class="horizontal-dl">
																							<dt><label>Vacancies</label></dt>
																						  <dd><?php echo $listingArray['vacancies']; ?></dd>
																					</dl>


                                        </div>

                                        <div class="col-md-6">
                                         <dl class="horizontal-dl">
																					 <dl class="horizontal-dl">
																							<dt><label>Frequency</label></dt>
																								<dd><?php echo $listingMetaArray[type] == "Once" ? "No" : $listingMetaArray[type]; ?></dd>
																						 </dl>

																						<!-- <dl class="horizontal-dl">
																							<dt><label>Start Date</label></dt>
																							 <dd><?php echo date('d-m-Y',strtotime($listingMetaArray[repeat_start])); ?></dd>
																						</dl> Don't display now -->


																					 <dl class="horizontal-dl">
																							<dt><label>Time</label></dt>
																							<dd><?php echo date('H:i',strtotime($listingMetaArray[start_time]))." - ". date('H:i',strtotime($listingMetaArray[end_time])) ; ?></dd>
																					 </dl>
																					<dl class="horizontal-dl">
																							<dt><label>Day(s)</label></dt>
																							<dd><?php
																							$weekDaysArray = explode(",", $listingMetaArray[weekly_days]);
																							$i = 0;
																							$len = count($weekDaysArray);
																							if (strlen($listingMetaArray[weekly_days])  == 0 && $listingMetaArray[type] == "Once")
																							{
																								$onceDay = date('w', $listingMetaArray[repeat_start]);
																								$weekDaysArray[0] = $onceDay;
																							}

																							foreach ($weekDaysArray as $weekDay) {
																								switch ($weekDay) {
																									case '0':
																										echo "Sunday";
																										break;
																									case '1':
																										echo "Monday";
																										break;
																									case '2':
																										echo "Tuesday";
																										break;
																									case '3':
																										echo "Wednesday";
																										break;
																									case '4':
																										echo "Thursday";
																										break;
																									case '5':
																										echo "Friday";
																										break;
																									case '6':
																										echo "Saturday";
																										break;
																									default:
																										# code...
																										break;
																								}

																								if ($i != $len-1 )
																								{
																									echo ", ";
																								}

																								$i++;
																							}

																							?></dd>
																					</dl>

                                        </div>




                                    </div>

                      <?php if($_POST['btn-action']=="view"): ?>

                            <div class="row" align="left" style="margin-top:20px;">
                            		<a class="btn btn-primary btn-lg btn-back" href="my-bookings.php">
                                               <i class="fa fa-angle-left"></i>
                                                <span>BACK</span>

                                                </a>
                            </div>

                      <?php elseif($_POST['btn-action']=="host-view"): ?>

                            <div class="row" align="left" style="margin-top:20px;">
                            		<a class="btn btn-primary btn-lg btn-back" href="my-activities.php">
                                               <i class="fa fa-angle-left"></i>
                                                <span>BACK</span>

                                                </a>
                            </div>

                      <?php else: ?>
                            <div class="row" align="right" style="margin-top:20px;">
                            		<form action="booking.php" method="post" >
                                            <input type="hidden" name="id" value="<?php echo $listingArray['id']; ?>">


    <?php if($listingArray['vacancies']==0):?>           <div class="alert alert-warning">
    <p>No vacancies left. </p>

    </div>
	<?php elseif($_SESSION['user_type_id']!=1): ?>


    <?php else: ?>

                                            <button class="btn btn-primary btn-lg" role="button" type="submit" >
                                                <span>BOOK NOW</span>
                                                <i class="fa fa-angle-right"></i>
                                                </button>

       <?php endif; ?>
						</form>

                            </div>

                      <?php endif; ?>


                            </div>
                 </div>
          </div>

     </div>
 </div>


 <?php include_once('includes/footer.php'); ?>
