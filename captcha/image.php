<?php

// Begin the session
session_start();

// If the session is not present, set the variable to an error message
if(!isset($_SESSION['captcha_id']))
{
    require '../includes/rand.php';
    $_SESSION['captcha_id'] = $str;
}
// Else if it is present, set the variable to the session contents
else
	$str = $_SESSION['captcha_id'];

// Set the content type
//header('Content-type: image/png');
header('Cache-control: no-cache');

// Create an image from button.png
$image = imagecreatefrompng('button.png');

// Set the font colour
$colour = imagecolorallocate($image, 0, 0, 0);

// Set the font
$font = 'fonts/rock.ttf';

// Set a random integer for the rotation between -15 and 15 degrees
$rotate = rand(0, 0);

// Create an image using our original image and adding the detail
imagettftext($image, 35, $rotate, 18, 60, $colour, $font, $str);

// Output the image as a png
imagepng($image);

?>