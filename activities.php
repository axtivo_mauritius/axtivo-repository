<?php 
session_start();
include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');
?>

<script type = "text/javascript">
  $(document).ready(function(){	
    $( "#date_from" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
      $("#date_from").valid();
    } });


    $( "#date_to" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
      $("#date_to").valid();
    } });
  });		  
</script>

<div class="inside-page">
  <div class="container">

    <h1>ACTIVITIES</h1>  

    <div class="row">

      <div class="col-md-3">
        <div class="panel panel-warning">

          <div class="panel-heading">
            <h3 class="panel-title">Filter By</h3>
          </div>

          <div class="panel-body">
            <form name="search_form" action="activities.php" method="post" id="search_form" />

              <label>Location</label>
              <select name="location" id="location" class="form-control" placeholder="Location">
                <option value="">All Locations</option>
<?php

                $locationSQL = mysqli_query($conn, "SELECT * FROM LOCATION");
                while ($locationArray = mysqli_fetch_assoc($locationSQL)) {

                  echo '<option value="'.$locationArray['id'].'" '.($_POST['location']==$locationArray['id']?"selected":"").'>'.$locationArray['location_name'].'</option>';

                }
?>
              </select>

              <br />


              <label>Category</label>

              <select name="activity" id="activity" class="form-control" placeholder="Activity">
                <option value="">All Activities</option>

<?php

                $categorySQL = mysqli_query($conn, "SELECT * FROM CATEGORY");
                while ($categoryArray = mysqli_fetch_assoc($categorySQL)) {

                  echo '<option value="'.$categoryArray['id'].'" '.($_POST['activity']==$categoryArray['id']?"selected":"").'>'.$categoryArray['category_name'].'</option>';

                }

?>
              </select>

              <br />


              <label>From</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="text" name="date_from" id="date_from" class="date-input  form-control" value="<?php echo $_POST['date_from']; ?>" />
              </div>


              <br />

              <label>To</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="text" name="date_to" id="date_to" class="date-input form-control" value="<?php echo $_POST['date_to']; ?>"/>
              </div>

              <br>


              <div align="center">
                <button type="submit" role="button" class="btn btn-primary" style="border-radius:30px;">

                <i class="fa fa-search"></i>
                </button>
              </div>

            </form>
          </div>
        </div>
      </div>


      <div class="col-md-9">

      <?php include_once('includes/activities_display.php'); ?>

      <!--<p class="alert alert-warning">No activities found.</p>-->
      </div>
    </div>

  </div>  
</div>


<?php include_once('includes/footer.php'); ?>