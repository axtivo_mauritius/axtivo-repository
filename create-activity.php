<?php session_start();

if(!isset($_SESSION['axt_auth_user']) || $_SESSION['user_type_id']!=2){
	die('Access Denied.');
}

include_once('includes/mydb.php');

include_once 'lib/dm/DataManager.php';

$model = "listing";
$id = isset($_POST['id'])?$_POST['id']:0;

$dm = new DataManager($model);
$dm->setItemsPerPage(0);
$dm->setPage(0);
if ($id) {
	$dm->setKeyValue($id);
}
$dm->setMode('single');
if ($dm->loadData() === false) {
	var_dump($dm->getErrors());
	exit;
}
$titles = $dm->getTitles();
$fields = $dm->getFields();
$fields = $fields[0];

if(isset($_GET['action']))
{
	//inserting/updating row
	if($_GET['action']=="save"){

		if(!$_POST){
			die('Action denied.');
		}

		$data = $_POST;

		$data['user_id']=$_SESSION['user_id'];

		if ($dm->save($data) === false) {
			var_dump($dm->getErrors());
			exit;
		}

		$newListingID = $dm->getKeyValue();
		$updateListing = mysqli_query($conn, "UPDATE LISTING set user_id=".$_SESSION['user_id']." where id=".$newListingID);

		header('location: my-activities.php?success');
		exit;
	}

}

include_once('includes/header.php');
include_once('includes/menu.php');
?>


<script>
$(document).ready(function(){

	$( "#inputStartsOn" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
		$("#inputStartsOn").valid();
	} });


	$( "#inputEndsOn" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
		$("#inputEndsOn").valid();
	} });

	$('.time').timepicker({
		'scrollDefault': 'now',
		'showDuration': true,
		'timeFormat': 'H:i',
		'step': 15
	});

	var time = document.getElementById('inputTime');
	var timeDatepair = new Datepair(time);

	$('#repeatOnEveryBox').hide();
	$('#repeatEveryBox').hide();
	$('#EndsOnBox').hide();
	$('#startOnLabel').text('Date: ');

	$('#selectRepeat').change(function(){

		var repeatType = $(this).val();

		if (repeatType == "daily")
		{
			$('#repeatOnEveryBox').hide();
			$('#repeatEveryBox').show();
			$('#repeatText').text("days");
			$('#startOnLabel').text('Starts On: ');
			$('#EndsOnBox').show();
		}
		else if ( repeatType == "weekly"){
			$('#repeatOnEveryBox').show();
			$('#repeatEveryBox').show();
			$('#repeatText').text("weeks");
			$('#startOnLabel').text('Starts On: ');
			$('#EndsOnBox').show();
		}
		else if ( repeatType == "monthly"){
			$('#repeatOnEveryBox').hide();
			$('#repeatText').text("months");
			$('#EndsOnBox').show();
			$('#startOnLabel').text('Date: ');
		}
		else if (repeatType == "once"){
			$('#repeatOnEveryBox').hide();
			$('#repeatEveryBox').hide();
			$('#EndsOnBox').hide();
			$('#startOnLabel').text('Date: ');
		}

	});


});
</script>

<div class="inside-page">
	<div class="container">
		<h1>Create Activity</h1>

		<form role="form" class="horizontal col-md-6" id="create-activity" action="create-activity-schedule.php?action=save" method="post" enctype="multipart/form-data">
			<input type="hidden"  name="id"  />

			<dl class="dl-horizontal">
				<dt><label for="listing_name">Listing Name</label></dt>
				<dd><input type="text"  name="listing_name"  class="form-control"  />
					&nbsp;</dd>
					<dd></dd>

				</dl>


				<dl class="dl-horizontal">
					<dt><label for="description">Description</label></dt>
					<dd><textarea  name="description"  class="form-control"  ></textarea>
						&nbsp;</dd>
						<dd></dd>

					</dl>


					<dl class="dl-horizontal">
						<dt><label for="image">Image</label></dt>
						<dd><input type="file"  name="image"  />
							&nbsp;</dd>
							<dd></dd>

						</dl>

						<dl class="dl-horizontal">
							<dt><label for="category_id">Category</label></dt>
							<dd><select  name="category_id" class="form-control"  />
								<?php

	 													$categorySQL = mysqli_query($conn, "SELECT * FROM CATEGORY");
	 													while ($categoryArray = mysqli_fetch_assoc($categorySQL)) {

	 																	echo '<option value="'.$categoryArray['id'].'" '.($_POST['activity']==$categoryArray['id']?"selected":"").'>'.$categoryArray['category_name'].'</option>';

	 													}

	 													?>
							</select>
							&nbsp;</dd>
							<dd></dd>

						</dl>

						<div id="StartsOnBox">
							<dl class="dl-horizontal">
								<dt><label for="inputStartsOn" id="startOnLabel">Date: </label></dt>
								<dd>
									<input class="form-control" id="inputStartsOn" name="inputStartsOn" type="text"/>
									&nbsp;</dd>

								</dl>
							</div>

							<div id="inputTime">
								<dl class="dl-horizontal">
									<dt><label for="inputTimeStart" >Start Time: </label></dt>
									<dd>
										<input class="form-control time start ui-timepicker-input" id="inputTimeStart" name="inputTimeStart" type="text"/>
										&nbsp;</dd>

									</dl>


									<dl class="dl-horizontal">
										<dt><label for="inputTimeEnd">End Time: </label></dt>
										<dd>
											<input class="form-control time end ui-timepicker-input" id="inputTimeEnd" name="inputTimeEnd" type="text"/>
											&nbsp;</dd>

										</dl>
									</div>


									<dl class="dl-horizontal">
										<dt><label for="selectRepeat">Repeats: </label></dt>
										<dd>
											<select class="form-control" name="selectRepeat" id="selectRepeat" required>
												<option value="once">No</option>
												<option value="weekly">Yes</option>
											</select>
										</dd>
									</dl>


<div id="repeatEveryBox">
									<dl class="dl-horizontal">
										<dt><label for="selectRepeatEvery">Repeat Every: </label></dt>
										<dd>
											<select class="form-control" id="selectRepeatEvery" name="selectRepeatEvery" required>
												<option value="1">1 week</option>
												<option value="2">2 weeks</option>
												<option value="3">3 weeks</option>
												<option value="4">4 weeks</option>
												<option value="5">5 weeks</option>
												<option value="6">6 weeks</option>
												<option value="7">7 weeks</option>
												<option value="8">8 weeks</option>
												<option value="9">9 weeks</option>
												<option value="10">10 weeks</option>
												<option value="11">11 weeks</option>
												<option value="12">12 weeks</option>
												<option value="13">13 weeks</option>
												<option value="14">14 weeks</option>
												<option value="15">15 weeks</option>
												<option value="16">16 weeks</option>
												<option value="17">17 weeks</option>
												<option value="18">18 weeks</option>
												<option value="19">19 weeks</option>
												<option value="20">20 weeks</option>
											</select>
										</dd>
									</dl>
								</div>

								<div id="repeatOnEveryBox">

									<dl class="dl-horizontal">
										<dt><label for="selectRepeatOn">Repeat on: </label></dt>
										<dd>
											<div class="checkbox">
												<label>
													<input name="inputRepeatOn[]" value="0" type="checkbox"> S 	&nbsp;
												</label>
												<label>
													<input name="inputRepeatOn[]" value="1" type="checkbox"> M 	&nbsp;
												</label>
												<label>
													<input name="inputRepeatOn[]" value="2" type="checkbox"> T 	&nbsp;
												</label>
												<label>
													<input name="inputRepeatOn[]" value="3" type="checkbox"> W 	&nbsp;
												</label>
												<label>
													<input name="inputRepeatOn[]" value="4" type="checkbox"> T 	&nbsp;
												</label>
												<label>
													<input name="inputRepeatOn[]" value="5" type="checkbox"> F 	&nbsp;
												</label>
												<label>
													<input name="inputRepeatOn[]" value="6" type="checkbox"> S 	&nbsp;
												</label>

											</div>

										</dd>
									</dl>
								</div>



									<dl class="dl-horizontal">
										<dt><label for="vacancies">Vacancies</label></dt>
										<dd><input type="number"  name="vacancies"  class="form-control"  />
											&nbsp;</dd>
											<dd></dd>

										</dl>

										<dl class="dl-horizontal">
											<dt><label for="location_id">Location</label></dt>
											<dd><select  name="location_id" class="form-control"  />
												<option value="1" >North</option>
												<option value="2" >South</option>
												<option value="3" >Central</option>
												<option value="4" >West</option>
											</select>
											&nbsp;</dd>
											<dd></dd>

										</dl>


										<dl class="dl-horizontal">
											<dt><label for="venue">Venue</label></dt>
											<dd><input type="text"  name="venue"  class="form-control"  />
												&nbsp;</dd>
												<dd></dd>

											</dl>


											<div class="form-group" id="EndsOnBox">
												<dl class="dl-horizontal">
													<dt><label for="inputEndsOn">End Date</label></dt>
													<dd>
														<input class="form-control" id="inputEndsOn" name="inputEndsOn" type="text"/>
														Only add an end date when your activity has a fixed ending scheduled
													</dd>

												</dl>
											</div>


											<div class="row">
												<button type="submit" class="btn btn-primary btn-lg" >Submit</button>
											</div>


										</form>

									</div>
								</div>


								<?php include_once('includes/footer.php'); ?>
