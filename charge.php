<?php
session_start();

if(!isset($_SESSION['axt_auth_user'])){
	die('You are not authorised to view this page.');
}

if(!isset($_POST['stripeToken'])){
	die("Access Denied.");
}

include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');
 
?>


<div class="inside-page">
    <div class="container">
    
    <?php 
	
	require 'lib/Stripe.php';

	// Set your secret key: remember to change this to your live secret key in production
	// See your keys here: https://dashboard.stripe.com/account/apikeys
	Stripe::setApiKey("sk_test_t68pIWdAScBEKagfFUqcHMIX");
	
	// Create a charge: this will charge the user's card
	try {
		
		$customerName = $_SESSION['firstname']." ".$_SESSION['surname'];
		
		 Stripe_Charge::create(array("amount" => 2999,
									"currency" => "eur",
									"card" => $_POST['stripeToken'],
									"description" => $customerName));
			
 
				//Save
				include_once 'lib/dm/DataManager.php';
				 
					$dm = new DataManager("subscription");
					
					$dm->setMode('single');
					
					if ($dm->loadData() === false) {
							var_dump($dm->getErrors());
							exit;
					}
					
				   $data = array();
				   
				    
				   $SQ=mysqli_query($conn, "select * from SUBSCRIPTION where user_id = ".$_SESSION['user_id']);
				   $rowSQ = mysqli_fetch_assoc($SQ);
				   
				   if(!empty($rowSQ)){
						$data['id'] = $rowSQ['id'];
					 }
				  
					
				   $data["subscription_name"] = "Monthly Plan";
				   $data["start_date"] = date('Y-m-d');
				   $data["expiration_date"] = date('Y-m-d', strtotime("+1 month"));
				   $data["credits_purchased"] = 31;
				   $data["credits_remaining"] = 31;
				   $data["user_id"] = $_SESSION['user_id'];
					 
				
					if ($dm->save($data) === false) {
							var_dump($dm->getErrors());
							exit;
					}
 
			
			echo '<h1>ORDER COMPLETE </h1>  
					
					<div class="alert alert-success">
               Your payment was successful.
				</div>';						
									
	 
	}  catch (Exception $e) {
		echo '<h1>PAYMENT FAILED </h1>  
		
		<div class="alert alert-danger">
				Your payment could not be processed.
				  </div>';
	  }
  
  ?>
        
         <a href="my-subscription.php" class="btn btn-default btn-lg">My Subscription</a> 
         
         
</div>
</div>         








 <?php include_once('includes/footer.php'); ?>