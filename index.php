<?php 
session_start();
unset($_SESSION['listing_id']);

include_once('includes/mydb.php');
include_once('includes/header.php');
include_once('includes/menu.php');

$textSQL = mysqli_query($conn, "SELECT * FROM STATIC_TEXT where id=6");
$textArray = mysqli_fetch_assoc($textSQL);
?>

<script>
$(document).ready(function(){	
 	$( "#date_from" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
        $("#date_from").valid();
    } });
		
		
		$( "#date_to" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
        $("#date_to").valid();
    } });
	
	$( "#mobile_date_from" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
        $("#mobile_date_from").valid();
    } });
	$( "#mobile_date_to" ).datepicker({dateFormat: 'dd-mm-yy'  , minDate: "+0d",onSelect: function () {
        $("#mobile_date_to").valid();
    } });
});		  
</script>
  
    
      <!-- Slider -->
 <div class="container-fluid" id="sliderbox">
 	<div class="row" id="slider">
    	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              
          
              
              	<div id="search"  class="hidden-xs hidden-sm">
                	<form name="search_form" action="activities.php" method="post" />
                	 <div class="row white-bar">
                     	<div class="col-md-3">
                        	<select name="location" id="location" class="form-control" placeholder="Location">
                               <option value="">All Locations</option>
                            	 <?php
											 
											 	$locationSQL = mysqli_query($conn, "SELECT * FROM LOCATION");
												while ($locationArray = mysqli_fetch_assoc($locationSQL)) {
														
														echo '<option value="'.$locationArray['id'].'">'.$locationArray['location_name'].'</option>';
													
												}
												
												?>
                            </select>
                        </div>
                        
                        <div class="col-md-3">
                        	<select name="activity" id="activity" class="form-control" placeholder="Activity">
                            	 <option value="">All Activities</option>
                                 
                                   <?php
											 
											 	$categorySQL = mysqli_query($conn, "SELECT * FROM CATEGORY");
												while ($categoryArray = mysqli_fetch_assoc($categorySQL)) {
														
														echo '<option value="'.$categoryArray['id'].'">'.$categoryArray['category_name'].'</option>';
													
												}
												
												?>
                            </select>
                        </div>
                        
                        <div class="col-md-6" id="date-div">
                        	<span>Between</span>
                            <input type="text" name="date_from" id="date_from" class="date-input" />
                            <i class="fa fa-calendar"> </i> - 
                            
                             
                            <input type="text" name="date_to" id="date_to" class="date-input" />
                            <i class="fa fa-calendar"> </i>  
                            
                            
                        </div>
                        
                        <button type="submit" class="submit_btn"> </button>
                        
                     </div>
                   </form>
                </div>
            
            
              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
              
              <div class="container">
            <div class="carousel-caption">
              <h1>LIST<br>
                    YOUR<br>
                    EMPTY SPACES
</h1>
              <p class="hidden-xs hidden-sm">Host listings for taster classes in Dublin. Join us and give it a try. Cancel anytime. </p>
             
              <p class="hidden-xs hidden-sm"><a role="button" href="register.php" class="btn btn-lg btn-primary"><span>GET STARTED</span> <i class="fa fa-angle-right"></i></a></p>
              
           
              
              
            </div>
          </div>
          
          
              
                
                <div class="item active">
                
                  <img src="images/slide-1.jpg" alt="Slide-1">
                </div>
               
            
                <div class="item">
                  <img src="images/slide-2.jpg" alt="Slide-2">
                </div>
                
                <div class="item">
                  <img src="images/slide-3.jpg" alt="Slide-3">
                </div>
              
          
             
              </div>
              
             
              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
		</div>
    </div>
</div>
  
     
 <!-- Welcome -->  
   
   <div class="container hidden-md hidden-lg" style="text-align:center;">
   		<h3 style="padding:10px;">Welcome to Axtivo, Dublin's activity community. Join us and give it a try. Cancel anytime. </h3>
   				
                    <p class="hidden-md hidden-lg" style="margin-bottom:35px;"><a role="button" href="register.php" class="btn btn-primary"><span>GET STARTED</span> <i class="fa fa-angle-right"></i></a></p>
   
   </div>
   
   
   
   
  <div id="search-mobile"  class="hidden-md hidden-lg">
  
                	<form name="search_form" action="activities.php" method="post" />
                	 
                     <div class="row">
                     
                     	<div class="col-xs-12">
                        	<select name="location" id="location" class="form-control" placeholder="Location">
                               <option value="">All Locations</option>
                            	 <?php
											 
											 	$locationSQL = mysqli_query($conn, "SELECT * FROM LOCATION");
												while ($locationArray = mysqli_fetch_assoc($locationSQL)) {
														
														echo '<option value="'.$locationArray['id'].'">'.$locationArray['location_name'].'</option>';
													
												}
												
												?>
                            </select>
                        </div>
                        
                        <div class="col-xs-12">
                        	<select name="activity" id="activity" class="form-control" placeholder="Activity">
                            	 <option value="">All Activities</option>
                                 
                                   <?php
											 
											 	$categorySQL = mysqli_query($conn, "SELECT * FROM CATEGORY");
												while ($categoryArray = mysqli_fetch_assoc($categorySQL)) {
														
														echo '<option value="'.$categoryArray['id'].'">'.$categoryArray['category_name'].'</option>';
													
												}
												
												?>
                            </select>
                        </div>
                        
                        <div class="col-xs-12">
                        <p style="color:#fff;">Between</p>
                        			 <div class="input-group">
                                          
											<input type="text" name="date_from" id="mobile_date_from" class="date-input  form-control"   />
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                          </div>
                        </div>
                        
                        <div class="col-xs-12">
                        <p style="color:#fff;">-</p>
                        		<div class="input-group">
                                          
											<input type="text" name="date_to" id="mobile_date_to" class="date-input form-control"   />
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                          </div>
                        </div>
                        
                        	 
                            
                            
                        </div>
                        
                        <div align="center">
                        <button type="submit" class="submit_btn"> </button>
                        </div>
                        
                     </div>
                   </form>
                   
                </div>
  	 
   

   
   
                
<!-- Members -->
  
   <div id="members">
  		 <div class="container">
              <div class="row sticker-heading">  
					<h1>MEMBERS</h1>
  				</div>
                
               <div class="row members-row">
               		<div class="col-md-6">
                    	<img src="images/dancer.jpg" class="dancer img img-responsive" />
                    </div>
                    <div class="col-md-6">
                    	<h2 id="join">Join <span class="bold">Axtivo</span></h2>
                        <p class="subheading">Get started in four easy steps</p>
                        
                        <ul class="circle-icons">
                        	<li><a href="register.php"><img src="images/register-icon.png" /> <span class="text">REGISTER</span></a></li>
                            <li><a href="activities.php"><img src="images/search-icon.png" /> <span class="text">SEARCH</span></a></li>
                            <li><a href="activities.php"><img src="images/book-icon.png" /> <span class="text">BOOK</span></a></li>
                            <li><img src="images/enjoy-icon.png" /> <span class="text">ENJOY</span></li>
                        </ul>
                    </div> 
               </div>
    	</div>
</div>

 

<!-- Trainers -->

<div id="trainers">
  		 <div class="container">
         		<div class="row sticker-heading">  
					<h1>HOSTING</h1>
  				</div>
                
                <div class="row trainer-text">
                    	<h2 id="join">Host for free on <span class="bold">Axtivo</span></h2>
                        <p class="subheading">Goodbye to empty spaces. Host our members and grow your business. </p>
                </div>  
                      
                      
                <div class="row testimonial hidden-xs hidden-sm">
                	<div class="col-md-7">
                    		<div class="box">
                            	<div class="image">
                                	<img src="graphics/<?php echo $textArray['image']; ?>" width="310" height="316" />
                                </div>
                                <div class="message">
                                	<h3><?php echo $textArray['page_title']; ?></h3>
                                   <?php echo htmlspecialchars_decode($textArray['content']); ?>
                                </div>
                            </div>
                             <p><a role="button" href="register.php" class="btn btn-lg btn-primary"><span>LIST YOUR ACTIVITY</span> <i class="fa fa-angle-right"></i></a></p>
                    </div>
                </div>
                
                 <div class="hidden-md hidden-lg">
                 		
                        <div class="col-xs-12">
                                	<img src="graphics/<?php echo $textArray['image']; ?>" class="img img-responsive" width="310" height="316"/>
                                </div>
                              <div class="message col-xs-12">
                                	<h3><?php echo $textArray['page_title']; ?></h3>
                                   <?php echo htmlspecialchars_decode($textArray['content']); ?>
                                   
                                    <p><a role="button" href="register.php" class="btn btn-primary"><span>LIST YOUR ACTIVITY</span> <i class="fa fa-angle-right"></i></a></p>
                                </div>   
                                
                 		
                         
                 </div>
 	
    
    </div>
</div>

<?php include_once('includes/footer.php'); ?>
