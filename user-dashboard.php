<?php
session_start();

include_once('includes/mydb.php');

if (!isset($_SESSION['axt_auth_user']))  {
		
		$email = $_SESSION['email']= $_POST['email'];
		$password=hash('sha512',$_POST['password']);
		
		$result = mysqli_query($conn, "SELECT * FROM USER WHERE email='".$email."' and password='".$password."'"); 
		
		$count=mysqli_num_rows($result);
		
		if($count==1){
	
			   $_SESSION['axt_auth_user'] = 1;
			   
			   session_regenerate_id();
			   
			   $userArray=mysqli_fetch_assoc($result);
				
				$_SESSION['user_id']=$userArray['id'];
				$_SESSION['firstname']=$userArray['firstname'];
				$_SESSION['surname']=$userArray['surname'];
				$_SESSION['telephone']=$userArray['telephone'];
				$_SESSION['address_1']=$userArray['address_1'];
				$_SESSION['address_2']=$userArray['address_2'];
				$_SESSION['business_name']=$userArray['business_name'];
				$_SESSION['website']=$userArray['website'];
				$_SESSION['status']=$userArray['status'];
				$_SESSION['user_type_id']=$userArray['user_type_id'];
 
		
		}
		else{
			unset($_SESSION['axt_auth_user']);
		}
	   
	 
 
}

 
include_once('includes/header.php');
include_once('includes/menu.php'); 
?>
<div class="inside-page">
    <div class="container">


				  
			<?php 
			 if (isset($_SESSION['axt_auth_user']) && $_SESSION['status']=="Active")
				{				
			 	
                  echo '<h1 class="col-xs-12">Dashboard</h1> 
				   
	
			  <ul class="thumbnails row col-xs-12" style="list-style:none;">';
			
          if($_SESSION['user_type_id']==1){ 
			 echo ' 
			 <li class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="thumbnail">
                  <div class="caption">
                    <h3>My Bookings</h3>
                    <p>View all your activities you have booked.</p>
                    <p><a class="btn btn-primary" href="my-bookings.php">View All</a> <a class="btn btn-default" href="activities.php">Book An Activity</a></p>
                  </div>
                </div>
              </li>';
		  }
		  
		  
		   if($_SESSION['user_type_id']==2){ 
		   
		    echo ' 
			 <li class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="thumbnail">
                  <div class="caption">
                    <h3>My Bookings</h3>
                    <p>View all your bookings.</p>
                    <p><a class="btn btn-primary" href="my-bookings.php">View All</a> </p>
                  </div>
                </div>
              </li>';
		 
		  
		  
			 echo ' 
			 <li class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="thumbnail">
                  <div class="caption">
                    <h3>My Activities</h3>
                    <p>List new activities or view existing. </p>
                    <p><a class="btn btn-primary" href="my-activities.php">View All</a> </p>
                  </div>
                </div>
              </li>';
		  }
			  
			 echo  '<li class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="thumbnail">
                  <div class="caption">
                    <h3>My Messages</h3>
                    <p>View all your messages.</p>
                    <p><a class="btn btn-primary" href="my-messages.php">View All</a>  </p>
                  </div>
                </div>
              </li>';
			  
		if($_SESSION['user_type_id']==1){
			  echo  '<li class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="thumbnail">
                  <div class="caption">
                    <h3>My Subscription</h3>
                    <p>View and renew your current subscription.</p>
                    <p><a class="btn btn-primary" href="my-subscription.php">Manage</a> </p>
                  </div>
                </div>
              </li> ';
		}
 	  
			 echo  '</ul>';
 
			}
			
			else {
			
			if (isset($_GET['logout'])) {
								echo '<h1>User</h1>
								<p><div class="well">
			 								<h4 class="alert-heading">Logout</h4>
											
			 								 	<p>You have been logged out. <p>
												<a href="#myModal" role="button" class="btn btn-default hidden-xs" data-toggle="modal">
												Login Again
												</a>
												   <a href="index.php" class="btn btn-default">Dismiss</a>
									</div></p>';
						}
			
			else {
				echo ' <h1>LOGIN</h1>
					
					<div class="alert alert-danger">
				  
						<h4 class="alert-heading">Authentication failed</h4>';
						
								
							if($_SESSION['axt_auth_user'] == 1 && $_SESSION['status']!='Active') {
							
									echo '<p>Your account is disabled. Please contact us for reactivation.  </p>';
							}
							
							else {
							
									echo '<p>The username or password entered is not correct.</p>';
							}
					echo '</div>';		
							
							echo '<p>
							  <a href="#myModal" role="button" class="btn btn-primary" data-toggle="modal">
							 	 Try Again
							  </a> 
							  <a href="index.php" class="btn btn-default">Dismiss</a>
							</p>
							
					 ';
		 		}
}
   ?>
		
         </div>  
 </div>
 		  
 <?php include_once('includes/footer.php'); ?>
